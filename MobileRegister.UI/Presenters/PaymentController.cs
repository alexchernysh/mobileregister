﻿using System;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Models;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Core;
using MVCSharp.Core.Tasks;
using MVCSharp.Core.Views;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.UI.Presenters
{
    public class PaymentController : ControllerBase
    {
        private string buffer = String.Empty;
        //регулярное выражение для десятичного числа
        private Regex DecimalNumber = new Regex(@"^\d{1,}(\.\d{0,2})?$"); 
        private PaymentModel _model = new PaymentModel();
        

        public override ITask Task
        {
            get { return base.Task; }
            set
            {
                base.Task = value;
            }
        }

        public override IView View
        {
            get
            {
                return base.View;
            }
            set
            {
                base.View = value;
                (View as IPaymentView).CancelButtonPressed += new EventHandler<EventArgs>(OnCancelButtonPressed);
                (View as IPaymentView).PrintButtonPressed+=new EventHandler<EventArgs>(OnPrintButtonPressed);
                (View as IPaymentView).CurChangeButtonPressed += new EventHandler<EventArgs>(OnCurChangeButtonPressed);
                (View as IPaymentView).PaymentViewActivated += new EventHandler<EventArgs>(OnPaymentViewActivated);
                (View as IPaymentView).DigitButtonPressed += new EventHandler<EventArgs>(OnDigitButtonPressed);
                (View as IPaymentView).BackspaceButtonPressed += new EventHandler<EventArgs>(OnBackspaceButtonPressed);
                (View as IPaymentView).EnterButtonPressed += new EventHandler<EventArgs>(OnEnterButtonPressed);
                (View as IPaymentView).PaymentLinesChanged += new EventHandler<EventArgs>(OnPaymentLinesChanged);
            }
        }

        void OnPaymentLinesChanged(object sender, EventArgs e)
        {
            UpdateLbl();
        }

        void OnPaymentViewActivated(object sender, EventArgs e)
        {
            (View as IPaymentView).TotalToPay = _model.GetTotalToPay().ToString("F2");
            (View as IPaymentView).CurrencyTotal = _model.GetBaseCur().name;
            UpdateLbl();            
        }

        void OnCurChangeButtonPressed(object sender, EventArgs e)
        {
            Currency_ currency = _model.NextCurrency();
            (View as IPaymentView).CurrencyTotal = currency.name;
        }

        void OnPrintButtonPressed(object sender, EventArgs e)
        {
            PrintReceipt();
        }

        void OnCancelButtonPressed(object sender, EventArgs e)
        {
            (View as IPaymentView).ClearPaymentLines();
            _model.ClearAllPayments();
            Task.Navigator.Navigate(MainTask.Sale);
        }



        void OnEnterButtonPressed(object sender, EventArgs e)
        {
            if ((buffer != String.Empty))
            {
                Double tmpPmt = Double.Parse(buffer);
                if (tmpPmt > 0)
                {
                    _model.AddPayment(tmpPmt);
                    UpdateLbl();
                    var cur = _model.CurrentCurrency();
                    (View as IPaymentView).AddPaymentLine(new string[] { tmpPmt.ToString(), cur.name, cur._desc });
                    clearCurrentPayment();
                }
            }
            else
            {
                OnPrintButtonPressed(this, null);
            }
        }
        void OnBackspaceButtonPressed(object sender, EventArgs e)
        {
            deleteLastSymbol();
        }

        void OnDigitButtonPressed(object sender, EventArgs e)
        {          
            processDigit(((DigitButtonEventArgs)e).buttonChar);
        }

        #region Utils

        private void PrintReceipt()
        {
            if (_model.IsPaymentEnough())
            {
                _model.CloseReceipt();
                (View as IPaymentView).ClearPaymentLines();
                (Task as MainTask).LastVisitedTarget = MainTask.Sale;
                (Task as MainTask).InfoMessage = "Печатается чек...";//не забудьте выдать сдачу
                Task.Navigator.Navigate(MainTask.Message);
            }
            else
            {
                (Task as MainTask).LastVisitedTarget = MainTask.Payment;
                (Task as MainTask).InfoMessage = "Оплата не достаточна...";
                Task.Navigator.Navigate(MainTask.Message);
            } 
        }

        /// <summary>
        /// Метод удаляет последний символ из буфера и обновляет view
        /// </summary>
        private void deleteLastSymbol()
        {
            if (buffer.Length > 0)
            {
                buffer = buffer.Remove(buffer.Length - 1, 1);
                (View as IPaymentView).CurrentPayment = "+ " + buffer;
            }
            //else

        }


        /// <summary>
        /// Процедура проверяет символ, и если это цифра, то символ добавляется к буферу, соответствующее
        /// поле в LoginView обновляется
        /// </summary>
        /// <param name="chr">char:символ с клавиатуры</param>
        private void processDigit(char chr)
        {
 
            if (!_model.IsPaymentEnough())
            {
                string tststr = addCharToBuffer(chr);
                if (DecimalNumber.IsMatch(tststr))
                {
                    buffer = tststr;
                    (View as IPaymentView).CurrentPayment = "+ " + buffer;
                }
            }
        }


        ///// <summary>
        ///// Процедура добавления символа в конец строки-буфера
        ///// </summary>
        ///// <param name="digit">char:добавляемый символ</param>
        //private void addCharToBuffer(char digit)
        //{

        //    var Builder = new StringBuilder(buffer);
        //    buffer = Builder.Append(digit).ToString();

        //}

        private string addCharToBuffer(char digit)
        {

            var Builder = new StringBuilder(buffer);
            return  Builder.Append(digit).ToString();

        }


        /// <summary>
        /// Очистка буфера и обновление View
        /// </summary>
        private void clearCurrentPayment()
        {
            buffer = String.Empty;
            (View as IPaymentView).CurrentPayment = "+ ";
            ///todo
            ///сделать недоступной кнопки ввод и отмена           

        }

        private void UpdateLbl()
        {
            Double rest = _model.Rest;
            Double change = _model.Change;
            (View as IPaymentView).PaidAmount = _model.Paid.ToString("F2");
            if ((rest > 0) || ((rest == 0) & (change == 0)))
            {
                (View as IPaymentView).ChangeRestAmount = rest.ToString("F2");
                (View as IPaymentView).ChangeRestName = "ОСТАТОК:";
            }
            else
                if (change > 0)
                {
                    (View as IPaymentView).ChangeRestAmount = change.ToString("F2");
                    (View as IPaymentView).ChangeRestName = "СДАЧА:";
                } 
            (View as IPaymentView).PaidAmount = _model.Paid.ToString("F2");
           
        }
        #endregion
    }
}
