﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Models;
using MobileRegister.UI.Misc;
using MVCSharp.Core;
using MVCSharp.Core.Views;
using MVCSharp.Core.Tasks;
using MobileRegister.Utility;

namespace MobileRegister.UI.Presenters
{
    public class LoginController:ControllerBase
    {
        private string buffer;

        private LoginModel _model=new LoginModel();


        public override ITask Task
        {
            get { return base.Task; }
            set
            {
                base.Task = value;
            }
        }

        public override IView View
        {
            get
            {
                return base.View;
            }
            set
            {
                base.View = value;
                (View as ILoginView).DigitButtonPressed += new EventHandler<EventArgs>(OnDigitButtonPressed);
                (View as ILoginView).InputButtonPressed += new EventHandler<EventArgs>(OnInputButtonPressed);
                (View as ILoginView).CancelButtonPressed += new EventHandler<EventArgs>(OnCancelButtonPressed);
                (View as ILoginView).LoginViewActivated+=new EventHandler<EventArgs>(OnViewActivated);
            }
        }

        public LoginController()
        {
        }

        void OnViewActivated(object sender, EventArgs e)
        {
            _model.crewNum = -1;
            //_model.Login();
            clearStaffNumber();
        }

        void OnCancelButtonPressed(object sender, EventArgs e)
        {
            if ((buffer.Length == 0) & (Task as MainTask).IsAdmin)
            {
                clearStaffNumber();
                //передать сообщение в View
                //временно - просто EXIT
                WinMenuSwitch.ShowTopStatusbar(true);
                (View as ILoginView).AppClose();

            }
            else
            {
                clearStaffNumber();
            }
        }

        void OnDigitButtonPressed(object sender, EventArgs e)
        {
            processDigit(((DigitButtonEventArgs)e).buttonChar);
        }

        private void OnInputButtonPressed(object sender, EventArgs e)
        {
            if (buffer.Length > 0)
            {
                _model.crewNum = int.Parse(buffer);
                _model.Login();
                if (_model.isLogged)
                {
                    clearStaffNumber();
                    (Task as MainTask).IsLogged = _model.isLogged;
                    (Task as MainTask).IsAdmin = _model.isAdmin;
                    Task.Navigator.Navigate(MainTask.MainMenu);

                  
                }
                else
                {
                    clearStaffNumber();
                }
            }
            //clearStaffNumber();
        }

        #region Utils

        /// <summary>
        /// Процедура проверяет символ, и если это цифра, то символ добавляется к буферу, соответствующее
        /// поле в LoginView обновляется
        /// </summary>
        /// <param name="chr">char:символ с клавиатуры</param>
        private void processDigit(char chr)
        {
            if (char.IsDigit(chr))
            {
                addCharToBuffer(chr);
                (View as ILoginView).StaffNumber = buffer;
            }
        }
        /// <summary>
        /// Процедура добавления символа в конец строки-буфера
        /// </summary>
        /// <param name="digit">char:добавляемый символ</param>
        private void addCharToBuffer(char digit)
        {            
            {
                var Builder = new StringBuilder(buffer);
                buffer = Builder.Append(digit).ToString();
            }
        }
        /// <summary>
        /// Очистка буфера и обновление View
        /// </summary>
        private void clearStaffNumber()
        {
            buffer = String.Empty;
            (View as ILoginView).StaffNumber = buffer;
            ///todo
            ///сделать недоступной кнопки ввод и отмена           

        }
        #endregion




    }
}
