﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Models;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Core;
using MVCSharp.Core.Tasks;
using MVCSharp.Core.Views;


namespace MobileRegister.UI.Presenters
{
    public class SaleController:ControllerBase
    {
        private string buffer = String.Empty;

        private SaleModel _model=new SaleModel();

        public override ITask Task
        {
            get { return base.Task; }
            set
            {
                base.Task = value;

            }
        }
        public override IView View
        {
            get
            {
                return (base.View);
            }
            set
            {
                base.View = value;
                
                (View as ISaleView).DigitButtonPressed += new EventHandler<EventArgs>(OnDigitButtonPressed);
                (View as ISaleView).ExitButtonPressed += new EventHandler<EventArgs>(OnExitButtonPressed);
                (View as ISaleView).BackspaceButtonPressed += new EventHandler<EventArgs>(OnBackspaceButtonPressed);
                (View as ISaleView).EnterButtonPressed += new EventHandler<EventArgs>(OnEnterButtonPressed);
                (View as ISaleView).PaymentButtonPressed+=new EventHandler<EventArgs>(OnPaymentButtonPressed);
                (View as ISaleView).ReceiptCancelButtonPressed += new EventHandler<EventArgs>(OnReceiptCancelButtonPressed);
                (View as ISaleView).ItemLinesChanged += new EventHandler<EventArgs>(OnItemLinesChanged);
                (View as ISaleView).SaleViewActivated += new EventHandler<EventArgs>(OnSaleViewActivated);
                (View as ISaleView).ItemMinusEvent += new EventHandler<EventArgs>(OnItemMinus);
                (View as ISaleView).ItemPlusEvent += new EventHandler<EventArgs>(OnItemPlus);
                (View as ISaleView).ItemDeleteEvent += new EventHandler<EventArgs>(OnItemDelete);
            }
        }

        void OnItemMinus(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void OnItemPlus(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void OnItemDelete(object sender, EventArgs e)
        {
            _model.DeleteItem(((ItemLineButtonEventArgs)e).Code);
            (View as ISaleView).DeleteItemLine();
        }

        void OnSaleViewActivated(object sender, EventArgs e)
        {
            try//if (_model.IsDBCreated())
            {
                (View as ISaleView).ClearItemLines();//очистить все товары с экрана
                //проверить наличие незакрытого чека
                if (_model.OpenReceiptExists())
                {
                    //(View as ISaleView).ClearItemLines();
                    //загрузить товары
                    foreach (var il in _model.GetCurrentReceipt().ItemLines)
                    {
                        (View as ISaleView).AddItemLine(new string[] { il.item.desc, 
                        il.item.Price[0].value.ToString("F2"), il.qty.ToString(),il.item.number });
                    }

                    //проверить, есть ли оплаты по этому чеку
                }
                else
                {                    
                    OnItemLinesChanged(this, EventArgs.Empty);
                }
            }
            catch
            {
                throw new Exception("Ошибка доступа к данным");
            }
            
        }

        void OnItemLinesChanged(object sender, EventArgs e)
        {
            (View as ISaleView).Info = String.Empty;
            (View as ISaleView).Total=_model.GetTotal().ToString("F2");
            (View as ISaleView).CurName = "EUR";
        }

        void OnReceiptCancelButtonPressed(object sender, EventArgs e)
        {
            _model.CancelReceipt();
            (View as ISaleView).ClearItemLines();//очистить все товары с экрана
            //остаться в состоянии продажи
            //увеличить номер чека
        }

        void OnPaymentButtonPressed(object sender, EventArgs e)
        {
            //(Task as MainTask).TotalToPay = _model.GetTotal();
            Task.Navigator.Navigate(MainTask.Payment);

           // _model.CloseReceipt();
            //(View as ISaleView).ClearItemLines();//пока  :очистить все товары с экрана

            //пока  :остаться в состоянии продажи
            //пока  :увеличить номер чекаа

        }

        void OnEnterButtonPressed(object sender, EventArgs e)
        {
            if (buffer != String.Empty)
            {
                var tmpItem = _model.AddItem(buffer);
                if (tmpItem.itemId > 0)
                {
                    (View as ISaleView).AddItemLine(new string[] { tmpItem.desc, tmpItem.Price[0].value.ToString("F2"), "1", tmpItem.number });
                    //(View as ISaleView).ReceiptNo=.......
                    clearItemCode();
                }
                else
                    if (tmpItem.itemId == 0)
                    {
                        (View as ISaleView).Info = String.Format("НЕИЗВЕСТНЫЙ КОД ТОВАРА: {0}", buffer);
                        clearItemCode();
                    }
                    else
                    {
                        (View as ISaleView).Info = String.Format("ТОВАРА {0} БОЛЬШЕ НЕТ!", buffer);
                        clearItemCode();
                    }
            }
            else
            {
                //если есть товары в списке - то перейти к оплате
                if (_model.GetTotal() > 0.0)
                    OnPaymentButtonPressed(this, null);                
            }
        }

        public SaleController()
        {
        }

        void OnExitButtonPressed(object sender, EventArgs e)
        {
            Task.Navigator.Navigate(MainTask.MainMenu);
        }

        void OnBackspaceButtonPressed(object sender, EventArgs e)
        {
            deleteLastSymbol();
        }

        void OnDigitButtonPressed(object sender, EventArgs e)
        {
            (View as ISaleView).Info = String.Empty;
            processDigit(((DigitButtonEventArgs)e).buttonChar);
        }

        #region Utils
        /// <summary>
        /// Метод удаляет последний символ из буфера и обновляет view
        /// </summary>
        private void deleteLastSymbol()
        {
            if (buffer.Length > 0)

            {
                buffer = buffer.Remove(buffer.Length-1, 1);
                (View as ISaleView).ItemCode = "Код: " + buffer;
            }
            //else

        }


        /// <summary>
        /// Процедура проверяет символ, и если это цифра, то символ добавляется к буферу, соответствующее
        /// поле в LoginView обновляется
        /// </summary>
        /// <param name="chr">char:символ с клавиатуры</param>
        private void processDigit(char chr)
        {
            //if (char.IsDigit(chr))
            //{
                addCharToBuffer(chr);
                (View as ISaleView).ItemCode = "Код: " + buffer;
            //}
        }


        /// <summary>
        /// Процедура добавления символа в конец строки-буфера
        /// </summary>
        /// <param name="digit">char:добавляемый символ</param>
        private void addCharToBuffer(char digit)
        {
            
                var Builder = new StringBuilder(buffer);
                buffer = Builder.Append(digit).ToString();
            
        }


        /// <summary>
        /// Очистка буфера и обновление View
        /// </summary>
        private void clearItemCode()
        {
            buffer = String.Empty;
            (View as ISaleView).ItemCode = "Код: ";
            ///todo
            ///сделать недоступной кнопки ввод и отмена           

        }
        #endregion



    }
}
