﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Models;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Core;
using MVCSharp.Core.Tasks;
using MVCSharp.Core.Views;


namespace MobileRegister.UI.Presenters
{
    public class MainMenuController:ControllerBase
    {
        public override ITask Task
        {
            get { return base.Task; }
            set
            {
                base.Task = value;

            }
        }
        public override IView View
        {
            get
            {
                return (base.View);
            }
            set
            {
                base.View = value;

                (View as IMainMenuView).MenuViewActivated += new EventHandler<EventArgs>(MenuViewActivated);
                (View as IMainMenuView).ExitButtonPressed+=new EventHandler<EventArgs>(MenuController_ExitButtonPressed);
                (View as IMainMenuView).LoadButtonPressed += new EventHandler<EventArgs>(MenuController_LoadButtonPressed);
                (View as IMainMenuView).UnloadButtonPressed+=new EventHandler<EventArgs>(MenuController_UnloadButtonPressed);
                (View as IMainMenuView).ReportsButtonPressed+=new EventHandler<EventArgs>(MenuController_ReportsButtonPressed);
                (View as IMainMenuView).SaleButtonPressed += new EventHandler<EventArgs>(MenuController_SaleButtonPressed);
            }
        }

        void MenuController_SaleButtonPressed(object sender, EventArgs e)
        {
            Task.Navigator.Navigate(MainTask.Sale);
        }

        void MenuController_ReportsButtonPressed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void MenuController_UnloadButtonPressed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void MenuController_LoadButtonPressed(object sender, EventArgs e)
        {
            Task.Navigator.Navigate(MainTask.LoadMenu);
        }

        void MenuController_ExitButtonPressed(object sender, EventArgs e)
        {            
            Task.Navigator.Navigate(MainTask.Login);
        }

        void MenuViewActivated(object sender, EventArgs e)
        {
            if ((Task as MainTask).IsAdmin)
            {
                (View as IMainMenuView).AdminRights();
            }
            else
            {
                (View as IMainMenuView).UserRights();
            }
            //(View as IMenuView).IsAdmin = (Task as MainTask).IsAdmin;
        }

        public MainMenuController()
        {
        }


    }
}
