﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Models;
using MobileRegister.UI.Misc;
using MVCSharp.Core;
using MVCSharp.Core.Views;
using MVCSharp.Core.Tasks;

namespace MobileRegister.UI.Presenters
{
    public class MessageController : ControllerBase
    {
        public override ITask Task
        {
            get { return base.Task; }
            set
            {
                base.Task = value;
            }
        }

        public override IView View
        {
            get
            {
                return base.View;
            }
            set
            {
                base.View = value;
                (View as IMessageView).MessageViewActivated += new EventHandler<EventArgs>(OnMessageViewActivated);
                (View as IMessageView).OkButtonPressed += new EventHandler<EventArgs>(OnOkButtonPressed);
            }
        }

        void OnOkButtonPressed(object sender, EventArgs e)
        {
            //(Task as MainTask).Navigator.Navigate(MainTask.MainMenu);
            (Task as MainTask).Navigator.Navigate((Task as MainTask).LastVisitedTarget);
        }

        void OnMessageViewActivated(object sender, EventArgs e)
        {
            (View as IMessageView).message = (Task as MainTask).InfoMessage;
        }
    }
}
