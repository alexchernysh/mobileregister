﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Models;
using MobileRegister.UI.Misc;
using MVCSharp.Core;
using MVCSharp.Core.Views;
using MVCSharp.Core.Tasks;

namespace MobileRegister.UI.Presenters
{
    public class LoadMenuController : ControllerBase
    {
        //private string buffer;

        private LoadModel _model;

      
        public override ITask Task
        {
            get { return base.Task; }
            set
            {
                base.Task = value;

            }
        }

        public override IView View
        {
            get
            {
                return base.View;
            }
            set
            {
                base.View = value;
                (View as ILoadMenuView).BackButtonPressed +=new EventHandler<EventArgs>(OnBackButtonPressed);
                (View as ILoadMenuView).FileNameAssigned +=new EventHandler<EventArgs>(OnFileNameAssigned);
                (View as ILoadMenuView).LoadNetButtonPressed +=new EventHandler<EventArgs>(OnLoadNetButtonPressed);
            }
        }

        void  OnLoadNetButtonPressed(object sender, EventArgs e)
        {
            _model = new LoadModel();
            ShowLoadResult();
        }

        void  OnFileNameAssigned(object sender, EventArgs e)
        {
            //(Task as MainTask).InfoMessage = "Загружается...";
            //Task.Navigator.Navigate(MainTask.Message);
            
            _model = new LoadModel((View as ILoadMenuView).fileName);
            ShowLoadResult();

        }

        void  OnBackButtonPressed(object sender, EventArgs e)
        {
            Task.Navigator.Navigate(MainTask.MainMenu);
        }

        void ShowLoadResult()
        {
            (Task as MainTask).LastVisitedTarget = MainTask.MainMenu;
            if (_model.SuccessLoad)
            {
                string pname = "№ "+_model.loadedSv.partner[0].PartNmb.ToString()+", "+ _model.loadedSv.partner[0].lName + " " + _model.loadedSv.partner[0].fName.Substring(0, 1) + ". " +
                    _model.loadedSv.partner[0].sName.Substring(0,1) + ". ";
                string msg = "Загружена ТТН №" + _model.loadedSv.number +
                    Environment.NewLine + "Рейс " + _model.loadedSv.flight.number + ", " +
                    Environment.NewLine + _model.loadedSv.flight.destination.fullName + ", " +
                    Environment.NewLine + _model.loadedSv.flight._date + ", " +
                    Environment.NewLine + pname;
               
                (Task as MainTask).InfoMessage = msg;    
                //Сообщить об этом, показать данные ТТН для контроля и перейти куда????
            }
            else
            {
                (Task as MainTask).InfoMessage = _model.ErrorMessage ;
               
                //Сообщить об этом, показать ошибку и предложить загрузиться еще раз????
            }
            Task.Navigator.Navigate(MainTask.Message);
        }
        public LoadMenuController ()
        {
        }

    }
}
