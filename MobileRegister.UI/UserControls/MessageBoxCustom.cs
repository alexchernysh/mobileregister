using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace ContactViewer
{
    public partial class MessageBoxCustom : Form, IDisposable
    {
        #region Consts

        private const int margin = 10;

        #endregion

        #region Variables

        private Bitmap outerBackground;
        private ImageAttributes transparency;
        private Rectangle rectangle;
        private RectangleF textLayoutRectangle;

        private Font font;
        private Brush brush;
        private bool isOuterBackgroundPainted = false;
         
        private ImageButton OK;
        private ImageButton Cancel;
        private PictureBox photo;

        private Bitmap background;
        private string message;

        #endregion

        #region Properties

        public Bitmap Background
        {
            set
            {
                background = value;
                rectangle = new Rectangle(
                    this.Width / 2 - value.Width / 2,
                    this.Height / 2 - value.Height / 2,
                    value.Width,
                    value.Height);
            }
        }

        /// <summary>
        /// Message for alert MessageBox
        /// </summary>
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                Size textSize = DrawingHelper.GetStringRectangle(this,message, font);
                float hSpan, vSpan;
                if (textSize.Width > rectangle.Width)
                    hSpan = 2;
                else
                    hSpan = (rectangle.Width - textSize.Width) / 2;
                hSpan += rectangle.Left;

                if (Controls.Contains(photo))
                    vSpan = photo.Bottom + margin;
                else
                    vSpan = margin;

                textLayoutRectangle = new RectangleF(
                    hSpan,
                    vSpan,
                    textSize.Width,
                    textSize.Height);
            }
        }

        /// <summary>
        /// Message for help popup
        /// </summary>
        public string Info
        {
            set
            {
                message = value;
                textLayoutRectangle = new RectangleF(
                    rectangle.Left + margin,
                    rectangle.Top + margin,
                    rectangle.Width - 2 * margin,
                    rectangle.Height - 2 * margin);
            }
        }

        #endregion

        /// <summary>
        /// MessageBox for Popup info (Help Form)
        /// </summary>
        public MessageBoxCustom(Bitmap background, Color transparentColor)
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.Size = Screen.PrimaryScreen.Bounds.Size;

            outerBackground = new Bitmap(this.Size.Width, this.Size.Height);
            Background = background;

            transparency = new ImageAttributes();
            transparency.SetColorKey(transparentColor, transparentColor);

            font = new Font("Arial", 12, FontStyle.Bold);
            brush = new SolidBrush(Color.Black);

            this.Click += delegate
            {
                this.Close();
            };
        }

        /// <summary>
        /// MessageBox with OK and Cancel buttons and Picture
        /// </summary>
        public MessageBoxCustom(Bitmap background, Bitmap bitmapOk, Bitmap bitmapOkPressed, Bitmap bitmapCancel, Bitmap bitmapCancelPressed, Color transparentColor, Bitmap picture)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.Size = Screen.PrimaryScreen.Bounds.Size;

            outerBackground = new Bitmap(this.Width, this.Height);
            Background = background;

            int heightToDrawButton = (int)rectangle.Bottom - bitmapOk.Height;

            transparency = new ImageAttributes();
            transparency.SetColorKey(transparentColor, transparentColor);

            font = new Font("Arial", 12, FontStyle.Bold);
            brush = new SolidBrush(Color.Black);

            OK = new ImageButton(bitmapOk, bitmapOkPressed);
            OK.Size = bitmapOk.Size;
            OK.Location = new Point(this.Width / 2 - (bitmapOk.Width + bitmapCancel.Width) / 2, heightToDrawButton);
            OK.Click += new EventHandler(Ok_Click);
            Controls.Add(OK);

            Cancel = new ImageButton(bitmapCancel, bitmapCancelPressed);
            Cancel.Size = bitmapCancel.Size;
            Cancel.Location = new Point(OK.Right, heightToDrawButton);
            Cancel.Click += new EventHandler(Cancel_Click);
            Controls.Add(Cancel);
            photo = new PictureBox();
            photo.Image = (Image)picture;
            photo.Size = new Size(70, 80);
            photo.SizeMode = PictureBoxSizeMode.StretchImage;
            photo.Location = new Point(
                Screen.PrimaryScreen.Bounds.Width / 2 - photo.Size.Width / 2,
                rectangle.Top + 10);
            Controls.Add(photo);

        }

        void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        void Ok_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!isOuterBackgroundPainted)
            {
                isOuterBackgroundPainted = true;
                DrawingHelper.DrawAlpha(e.Graphics, outerBackground, 180, 0, 0);
            }
            e.Graphics.DrawImage(background, rectangle, 0, 0, background.Width, background.Height, GraphicsUnit.Pixel, transparency);
            e.Graphics.DrawString(message, font, brush, textLayoutRectangle);
            base.OnPaint(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // Do nothing
        }
        
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (outerBackground != null)
                outerBackground.Dispose();
            if (transparency != null)
                transparency.Dispose();
            if (font != null)
                font.Dispose();
            if (brush != null)
                brush.Dispose();

            base.Dispose(true);
        }

        #endregion
    }
}