﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bornander.UI;
using MobileRegister.UI.Properties;
using MobileRegister.UI.Misc;

namespace MobileRegister.UI.UserControls
{
    public partial class ItemLine : UserControl,IExtendedListItem
    {
        private bool isSelected = false;
        private bool onOddRow = false;
        private String pPlu;
        private int pQty;
        private double pPrice;
        private ImageButton delButton;

        private ImageButton plusButton;
        private ImageButton minusButton;

        private ImageButton cancelButton;
        private ImageButton deleteButton;



        public event EventHandler<EventArgs> plusButtonPressed;

        public event EventHandler<EventArgs> minusButtonPressed;

        public event EventHandler<EventArgs> delButtonPressed;


        public ItemLine()
        {
            InitializeComponent();
        }

        public ItemLine(string plu, string desc, string price, string qty)
        {
            InitializeComponent();

            this.pPlu = plu;
            this.pQty = int.Parse(qty);
            this.pPrice = double.Parse(price);

            string plu_desc = plu.PadLeft(6, '0') + " - " + desc;

            this.lblItemPluDesc.Text = plu_desc.Length > 35 ? plu_desc.Substring(0, 34) : plu_desc;
            this.lblItemPrice.Text = price + " €";
            this.lblItemQty.Text = qty+" шт.";

            SetSelectedState(false);
            this.btnDelete.Visible = false;

            #region ImageButtons

            this.SuspendLayout();

            deleteButton = new ImageButton(Resources.buttonDelete, Resources.buttonDeletePressed);
            deleteButton.Location = new System.Drawing.Point(20, 29);
            deleteButton.Size = new System.Drawing.Size(90, 30);
            deleteButton.Visible = false;
            this.Controls.Add(deleteButton);
            deleteButton.Click += new EventHandler(deleteButton_Click);

            cancelButton = new ImageButton(Resources.buttonDelCancel, Resources.buttonDelCancelPressed);
            cancelButton.Location = new System.Drawing.Point(130, 29);
            cancelButton.Size = new System.Drawing.Size(90, 30);
            cancelButton.Visible = false;
            this.Controls.Add(cancelButton);
            cancelButton.Click += new EventHandler(cancelButton_Click);


            delButton = new ImageButton( Resources.buttonDelYellow,Resources.buttonDelPressedYellow );
            delButton.Location = new System.Drawing.Point(190, 39);
            delButton.Size = new System.Drawing.Size(45, 30);
            this.Controls.Add(delButton);
            delButton.Click += new EventHandler(delButton_Click);

            plusButton = new ImageButton(Resources.buttonPlusYellow, Resources.buttonPlusPressedYellow);
            plusButton.Location = new System.Drawing.Point(5, 39);
            plusButton.Size = new System.Drawing.Size(45, 30);
            this.Controls.Add(plusButton);
            plusButton.Click += new EventHandler(plusButton_Click);

            minusButton = new ImageButton(Resources.buttonMinusYellow, Resources.buttonMinusPressedYellow);
            minusButton.Location = new System.Drawing.Point(55, 39);
            minusButton.Size = new System.Drawing.Size(45, 30);
            this.Controls.Add(minusButton);
            minusButton.Click += new EventHandler(minusButton_Click);

            this.ResumeLayout();

            #endregion


        }

        void cancelButton_Click(object sender, EventArgs e)
        {
            SetDeleteQuestionState(false);
            //SelectedChanged(false);
        }

        void deleteButton_Click(object sender, EventArgs e)
        {
            if (delButtonPressed != null)
                delButtonPressed(this, new ItemLineButtonEventArgs(this.pPlu));
        }

        void minusButton_Click(object sender, EventArgs e)
        {
            if (minusButtonPressed != null)
                minusButtonPressed(this, new ItemLineButtonEventArgs(this.pPlu));
        }

        void plusButton_Click(object sender, EventArgs e)
        {
            if (plusButtonPressed != null)
                plusButtonPressed(this, new ItemLineButtonEventArgs(this.pPlu));
        }

        void delButton_Click(object sender, EventArgs e)
        {
            SetDeleteQuestionState(true);
        }

        private void SetDeleteQuestionState(bool cond)
        {
            if (delButton != null) //написать по-другому, чтобы не надо было тут проверять
            {
                this.SuspendLayout();
                delButton.Visible = !cond;
                minusButton.Visible = !cond;
                plusButton.Visible = !cond;

                //lblItemPluDesc.Visible = !cond;
                lblItemPrice.Visible = !cond;
                lblItemQty.Visible = !cond;

                deleteButton.Visible = cond;
                cancelButton.Visible = cond;
                this.ResumeLayout();
            }
        }

        private void SetSelectedState(bool isSelected)
        {
            if (isSelected)

                this.Size = new Size(240, 71);
            else
            {
                this.Size = new Size(240, 40);
                SetDeleteQuestionState(false);
            }

           


        }

        private void SetBackColor()
        {
            if (isSelected)
                BackColor = Color.Yellow;
            else
                BackColor = onOddRow ? Color.LightYellow : Color.LightGray;
        }

        #region IExtendedListItem Members

        public void SelectedChanged(bool isSelected)
        {
            this.isSelected = isSelected;
            SetSelectedState(isSelected);

            SetBackColor();
        }

        public void PositionChanged(int index)
        {
            onOddRow = (index & 1) == 0;
            SetBackColor();
        }

        #endregion

        #region Public properties

        public string ItemPluDesc
        {
            get { return lblItemPluDesc.Text; }
        }
        public string ItemPlu
        {
            get { return pPlu; }
        }

        public string Price
        {
            get { return lblItemPrice.Text; }
        }

        public int Qty
        {
            get { return pQty; }
            set { 
                    pQty = value;
                    lblItemQty.Text = pQty.ToString() + " шт.";
                }
        }

        #endregion


        
    }
}
