﻿namespace MobileRegister.UI.UserControls
{
    partial class ItemLine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblItemPluDesc = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblItemPrice = new System.Windows.Forms.Label();
            this.lblItemQty = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lblItemPluDesc
            // 
            this.lblItemPluDesc.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblItemPluDesc.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblItemPluDesc.Location = new System.Drawing.Point(0, 0);
            this.lblItemPluDesc.Name = "lblItemPluDesc";
            this.lblItemPluDesc.Size = new System.Drawing.Size(240, 20);
            this.lblItemPluDesc.Text = "<plu - description>";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(200, 42);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(32, 32);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Visible = false;
            // 
            // lblItemPrice
            // 
            this.lblItemPrice.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblItemPrice.ForeColor = System.Drawing.Color.DarkRed;
            this.lblItemPrice.Location = new System.Drawing.Point(0, 19);
            this.lblItemPrice.Name = "lblItemPrice";
            this.lblItemPrice.Size = new System.Drawing.Size(63, 20);
            this.lblItemPrice.Text = "<price>";
            // 
            // lblItemQty
            // 
            this.lblItemQty.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblItemQty.ForeColor = System.Drawing.Color.DarkRed;
            this.lblItemQty.Location = new System.Drawing.Point(69, 19);
            this.lblItemQty.Name = "lblItemQty";
            this.lblItemQty.Size = new System.Drawing.Size(62, 19);
            this.lblItemQty.Text = "<qty>";
            this.lblItemQty.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 1);
            // 
            // ItemLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblItemQty);
            this.Controls.Add(this.lblItemPrice);
            this.Controls.Add(this.lblItemPluDesc);
            this.Name = "ItemLine";
            this.Size = new System.Drawing.Size(240, 40);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblItemPluDesc;
        private System.Windows.Forms.Label lblItemPrice;
        private System.Windows.Forms.Label lblItemQty;
        private System.Windows.Forms.Panel panel1;


    }
}
