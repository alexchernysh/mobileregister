﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bornander.UI;
using MobileRegister.UI.Properties;
using MobileRegister.UI.Misc;


namespace MobileRegister.UI.UserControls
{
    public partial class PaymentLine : UserControl, IExtendedListItem
    {
        private bool isSelected = false;
        private bool onOddRow = false;
        private string pCurName;
        private string pCurDesc;
        private double pLocalAmount;
        private double pBaseAmount;
        private ImageButton delButton;
        private ImageButton cancelButton;
        private ImageButton deleteButton;
        public event EventHandler<EventArgs> delButtonPressed;



        public PaymentLine()
        {
            InitializeComponent();
        }

        public PaymentLine(Double localvalue, string curname, string curdesc)
        {
            InitializeComponent();

            pLocalAmount = localvalue;
            pCurDesc = curdesc;
            pCurName = curname;

            //lblBaseAmount.Text = String.Empty;
            lblCurrencyName.Text = pCurDesc;
            lblLocalAmount.Text = pLocalAmount.ToString("F2") + " " + pCurName;
            SetSelectedState(false);

            #region ImageButtons

            this.SuspendLayout();

            deleteButton = new ImageButton(Resources.buttonDelete, Resources.buttonDeletePressed);
            deleteButton.Location = new System.Drawing.Point(20, 29);
            deleteButton.Size = new System.Drawing.Size(90, 30);
            deleteButton.Visible = false;
            this.Controls.Add(deleteButton);
            deleteButton.Click += new EventHandler(deleteButton_Click);

            cancelButton = new ImageButton(Resources.buttonDelCancel, Resources.buttonDelCancelPressed);
            cancelButton.Location = new System.Drawing.Point(130, 29);
            cancelButton.Size = new System.Drawing.Size(90, 30);
            cancelButton.Visible = false;
            this.Controls.Add(cancelButton);
            cancelButton.Click += new EventHandler(cancelButton_Click);


            delButton = new ImageButton(Resources.buttonDelYellow, 
                Resources.buttonDelPressedYellow);
            delButton.Location = new System.Drawing.Point(190, 39);
            delButton.Size = new System.Drawing.Size(45, 30);
            this.Controls.Add(delButton);
            delButton.Click += new EventHandler(delButton_Click);

            this.ResumeLayout();

            #endregion
            
        }


        void deleteButton_Click(object sender, EventArgs e)
        {
            if (delButtonPressed != null) { };
               // delButtonPressed(this, new ItemLineButtonEventArgs(this.pPlu));
        }



        void cancelButton_Click(object sender, EventArgs e)
        {
            SetDeleteQuestionState(false);
            //SelectedChanged(false);
        }


        void delButton_Click(object sender, EventArgs e)
        {
            SetDeleteQuestionState(true);
        }

        private void SetDeleteQuestionState(bool cond)
        {
            if (delButton != null) //написать по-другому, чтобы не надо было тут проверять
            {
                this.SuspendLayout();
                delButton.Visible = !cond;


                //lblBaseAmount.Visible = !cond;
                lblLocalAmount.Visible = !cond;

                deleteButton.Visible = cond;
                cancelButton.Visible = cond;
                this.ResumeLayout();
            }
        }

        private void SetSelectedState(bool isSelected)
        {
            //if this.index=
            //if (isSelected)

            //    this.Size = new Size(240, 71);
            //else
            //{
            //    this.Size = new Size(240, 40);
            //    SetDeleteQuestionState(false);
            //}

        }

        private void SetBackColor()
        {
            if (isSelected)
                BackColor = Color.Yellow;
            else
                BackColor = onOddRow ? Color.LightYellow : Color.LightGray;
        }

        #region IExtendedListItem Members

        public void SelectedChanged(bool isSelected)
        {
            this.isSelected = isSelected;
            //SetSelectedState(isSelected);
            SetBackColor();
        }

        public void PositionChanged(int index)
        {

            onOddRow = (index & 1) == 0;
            SetBackColor();
        }

        #endregion

        #region Public properties

        public string CurrencyName
        {
            get { return pCurName; }
        }

        public Double SumLocal
        {
            get { return this.pLocalAmount; }
            set
            {
                pLocalAmount = value;
                lblLocalAmount.Text = pLocalAmount.ToString("F2") + " " + pCurName;
            }
        }

        //public Double SumBase
        //{
        //    get { return this.pBaseAmount; }
        //}

        

        #endregion
    }
}
