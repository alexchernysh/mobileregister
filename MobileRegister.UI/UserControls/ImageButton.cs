using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace MobileRegister.UI.UserControls
{
    class ImageButton : Control
    {
        #region Variables
        private ImageAttributes imageAttr = new ImageAttributes();
        private Image image, imagePressed,imageDisabled;
        private bool isPressed;
        private bool drawText = false;

        #endregion

        public bool DrawText
        {
            get { return drawText; }
            set { drawText = value; }
        }
        public ImageButton(Bitmap image, Bitmap imagePressed, Bitmap imageDisabled)
        {
            //imageAttr.SetColorKey(Color.White, Color.White);
            isPressed = false;

            this.image = (Bitmap)image;
            this.imagePressed = (Bitmap)imagePressed;
            this.imageDisabled = (Bitmap)imageDisabled;

            this.Size = new Size(image.Width, image.Height);
        }

        public ImageButton(Bitmap image, Bitmap imagePressed)
        {
            imageAttr.SetColorKey(Color.White, Color.White);
            isPressed = false;

            this.image = (Bitmap)image;
            this.imagePressed = (Bitmap)imagePressed;
            this.imageDisabled = (Bitmap)image;

            this.Size = new Size(image.Width, image.Height);
        }

        #region Custom Graphics

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            isPressed = true;
            this.Invalidate(); // Force the refresh of the UI
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            isPressed = false;
            this.Invalidate();
            base.OnMouseUp(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //Do nothing, in order to avoid flickering
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            //imageAttr.SetColorKey(Color.Red,Color.Red);
            //Rectangle imgRect=new Rectangle(0,0,image.Width,image.Height);
            if (this.Enabled)
            {
                if (isPressed)
                    e.Graphics.DrawImage(imagePressed, 0, 0);//imgRect, 0, 0,
                //image.Width,image.Height,GraphicsUnit.Pixel,imageAttr );
                else
                {
                    e.Graphics.DrawImage(image, 0, 0);
                }
                    
                //imgRect, 0, 0,
                //image.Width, image.Height, GraphicsUnit.Pixel, imageAttr);
            }
            else
                e.Graphics.DrawImage(imageDisabled, 0,0);//imgRect, 0, 0,
               //image.Width, image.Height, GraphicsUnit.Pixel, imageAttr);

            if (drawText && (this.Text.Length > 0))
            {
                SizeF size = e.Graphics.MeasureString(this.Text, this.Font);

                // Center the text inside the client area of the PictureButton.
                e.Graphics.DrawString(this.Text,
                    this.Font,
                    new SolidBrush(this.ForeColor),
                    (this.ClientSize.Width - size.Width) / 2,
                    (this.ClientSize.Height - size.Height) / 2);
            }
            base.OnPaint(e);

        }

        #endregion

    }
}