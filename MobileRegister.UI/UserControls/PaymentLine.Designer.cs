﻿namespace MobileRegister.UI.UserControls
{
    partial class PaymentLine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLocalAmount = new System.Windows.Forms.Label();
            this.lblCurrencyName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lblLocalAmount
            // 
            this.lblLocalAmount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.lblLocalAmount.ForeColor = System.Drawing.Color.Black;
            this.lblLocalAmount.Location = new System.Drawing.Point(0, 0);
            this.lblLocalAmount.Name = "lblLocalAmount";
            this.lblLocalAmount.Size = new System.Drawing.Size(121, 20);
            this.lblLocalAmount.Text = "<Local>";
            // 
            // lblCurrencyName
            // 
            this.lblCurrencyName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblCurrencyName.Location = new System.Drawing.Point(0, 20);
            this.lblCurrencyName.Name = "lblCurrencyName";
            this.lblCurrencyName.Size = new System.Drawing.Size(240, 20);
            this.lblCurrencyName.Text = "<CurrencyName>";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 1);
            // 
            // PaymentLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(110F, 110F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblLocalAmount);
            this.Controls.Add(this.lblCurrencyName);
            this.Name = "PaymentLine";
            this.Size = new System.Drawing.Size(240, 40);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLocalAmount;
        private System.Windows.Forms.Label lblCurrencyName;
        private System.Windows.Forms.Panel panel1;
    }
}
