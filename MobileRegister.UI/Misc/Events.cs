﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace MobileRegister.UI.Misc
{
    class DigitButtonEventArgs:EventArgs
    {
        public char buttonChar { get; set; }
        public DigitButtonEventArgs(char buttonchar)
        {
            this.buttonChar = buttonchar;
        }
    }
    class ItemLineButtonEventArgs : EventArgs
    {
        public string Code { get; set; }
        public ItemLineButtonEventArgs(string code)
        {
            this.Code = code;
        }
    }
}
