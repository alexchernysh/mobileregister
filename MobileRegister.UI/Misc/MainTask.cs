﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MVCSharp.Core.Configuration.Tasks;
using MVCSharp.Core.Tasks;
using MobileRegister.UI.Presenters;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.UI.Misc
{
    public class MainTask:TaskBase
    {
        public bool IsLogged
        { get; set; }

        public bool IsAdmin
        { get; set; }

        public Partner LoggedPartner
        { get; set; }

        public string InfoMessage
        { get; set; }

        public string LastVisitedTarget
        { get; set; }

        //public Double TotalToPay
        //{ get; set; }


        //[InteractionPoint(typeof(LoginController), Sale)]
       // public const string Login = "Login";

        [InteractionPoint(typeof(LoginController), true)]
        public const string Login = "Login";

        [InteractionPoint(typeof(SaleController), true)]
        public const string Sale = "Sale";

        [InteractionPoint(typeof(MainMenuController), true)]
        public const string MainMenu = "MainMenu";

        [InteractionPoint(typeof(LoadMenuController), true)]
        public const string LoadMenu = "LoadMenu";

        [InteractionPoint(typeof(MessageController), true)]
        public const string Message = "Message";

        [InteractionPoint(typeof(PaymentController), true)]
        public const string Payment = "Payment";

        public override void OnStart(object param)
        {
            Navigator.NavigateDirectly(Login);
        }
    }
}
