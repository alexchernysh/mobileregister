﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.Utility;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.UI.Models
{
    class LoadModel
    {
        private bool successLoad;
        public string ErrorMessage
        {
            get;
            set;
        }
        public Sv loadedSv //заменить на ФИО табельный номер и т.д.
        {
            get;
            set;
        }
        private WayBillLoader wbldr = new WayBillLoader();
        //private DataController dtctrl = new DataController();
        public bool SuccessLoad
        {
            get {return successLoad;}           
        }


        public LoadModel()
        {
            //temporary  hardcoded svmun 

            //try
            //{
            //    int svnum = 344065;
            //    loadedSv = wbldr.Load(svnum);
            //    ClearData();
            //    InsertData();
            //    successLoad = true;
            //}
            //catch (Exception ex)
            //{
            //    ErrorMessage = ex.Message;
            //    successLoad = false;
            //}

        }

        public LoadModel(string wbpath)
        {
            //try
            //{
                loadedSv = wbldr.Load(wbpath);
                CreateDb();
                InsertData();
                successLoad = true;
            //}
            //catch (Exception ex)
            //{
            //    ErrorMessage = ex.Message;
            //    successLoad = false;
            //}
        }

        private void InsertData()
        {
            DataController.InitialLoadData(loadedSv);
        }

        private void CreateDb()
        {
            DataController.CreateDB();
        }
    }
}
