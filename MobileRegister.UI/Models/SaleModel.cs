﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.Domain.CriteriaLib;
using MobileRegister.Utility;

namespace MobileRegister.UI.Models
{
    class SaleModel
    {
        //private DataController dtctrl = new DataController();
        public string ReceiptNumber
        {
            get {return GetLastReceiptNumber(); }
        }

        public bool OpenReceiptExists()
        {
            return DataController.IsCurrentReceiptOpen();
        }

        public Receipt GetCurrentReceipt()
        {
            return DataController.GetCurrentReceipt();
        }

        private string GetLastReceiptNumber()
        {
            return DataController.CurrentReceiptNumber.ToString();
        }
        public SvItem  AddItem(string code)
        {
            return DataController.AddItem(code); 
        }

        public bool IsEnoughQty(string plu)
        {
            return DataController.IsEnoughQty(plu, 1);
        }

        public void DeleteItem(string code)
        {
            DataController.DeleteItem(code);
        }

        public void CancelReceipt()
        {
            DataController.CancelReceipt();
        }

        public void CloseReceipt()
        {

        }

        public double GetTotal()
        {
            return DataController.GetTotal();
        }

    }
}
