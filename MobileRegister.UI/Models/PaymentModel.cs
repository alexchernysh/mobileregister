﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Abstract;
using MobileRegister.Domain.ConstLib;
using MobileRegister.Domain.CriteriaLib;
using MobileRegister.Utility;

namespace MobileRegister.UI.Models
{
    class PaymentModel
    {
        private int CurrentCur = 0;
        private List<Currency_> CurrencyList;
        //private DataController dtctrl = new DataController();
        private ReceiptPrinter rprt = new ReceiptPrinter();
        private PaymentCalculator pcalc;

        public PaymentModel()
        {
            CurrencyList = DataController.GetCurrencyDict();
        }

        public Double GetTotalToPay()
        {
            Double total = DataController.GetTotal();
            pcalc = new PaymentCalculator(total);
            return total;
        }

        public void ResetCounter()
        {
            CurrentCur = 0;
        }

        public Currency_ GetBaseCur()
        {
            return CurrencyList[0];
            //var c_ = from cr in CurrencyList where cr.isBase select cr;
             
            //return (Currency_) c_;
            //return dtctrl.GetBaseCurrency().name;
        }


        /// <summary>
        /// Увеличивает внутренний счетчик текущей валюты, возвращает следующую валюту в списке
        /// </summary>
        /// <returns></returns>
        public Currency_ NextCurrency()
        {
            CurrentCur=(CurrentCur==CurrencyList.Count-1)?0:CurrentCur+1;
            return CurrencyList[CurrentCur];
        }

        public Currency_ CurrentCurrency()
        {
            return CurrencyList[CurrentCur];
        }

        /// <summary>
        /// Временный метод для печати и закрытия чека
        /// </summary>
        public void CloseReceipt()
        {
            var rcpt = DataController.GetCurrentReceipt();
            rprt.AddString("#" + rcpt.id.ToString()+" "+rcpt.timestamp);
            foreach (SvItemLine line in rcpt.ItemLines)
            {
                rprt.AddString( String.Format( "{0} {1} {2} {3}", line.item.number, line.item.desc, 
                    line.qty.ToString(),(line.item.Price[0].value*line.qty).ToString()));
            }
            //если чек по каким-то причинам не распечатан, то его можно будет распечатать из меню
            rprt.PrintReceipt();
            DataController.CloseReceipt();
        }
        /// <summary>
        /// Добавляет в стек еще одну оплату; валюта - текущая валюта CurrentCurrency;
        /// </summary>
        /// <param name="payment"></param>
        public void AddPayment(Double money)
        {
            var pmt=new Payment( money, CurrentCurrency());
            pcalc.PutPayment(pmt);
            DataController.AddPayment(pmt);
        }

        public void ClearAllPayments()
        {
            DataController.DeletePayments();
        }

        public bool IsPaymentEnough()
        {
            return pcalc.GetRest(CurrentCurrency()) == 0;
        }

        public Double Rest
        {
            get { return pcalc.GetRest(GetBaseCur()); }
        }

        public Double Change
        {
            get { return pcalc.GetChange(GetBaseCur()); }
        }

        public Double Paid
        {
            get { return pcalc.GetPaidSum(); }
        }


    }
}
