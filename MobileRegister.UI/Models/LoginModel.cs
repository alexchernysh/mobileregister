﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.UI.Abstract;
using MobileRegister.Domain.ConstLib;
using MobileRegister.Domain.CriteriaLib;
using MobileRegister.Utility;

namespace MobileRegister.UI.Models
{
    public class LoginModel
    {
        
        private bool islogged = false;
        private int crewnum = 0;
        private Partner user;

        //public event Action<bool> LoginEvent;

        //private DataController dtctrl = new DataController();
        public LoginModel()
        {
            //need to load Partner data from the database
            //user = new Partner { PartNmb = dtctrl.GetParnerNumber() };
        }

        public void Login()
        {
            //crewNum = crewnum;
            //bool result = false;
            if (crewnum == MiscConst.adminCrewNum)
            {
                islogged = true;
            }
            else
            {
                //if (DataController.IsDBCreated())
                //{
                    user = new Partner() { PartNmb = DataController.GetParnerNumber() };
                    islogged = (crewnum == user.PartNmb);
                //}
                //else
                //{
                //    user = new Partner() { PartNmb = -1 };
                //}
            }
        }

        public int crewNum
        {
            set { crewnum = value; }
        }


        /// <summary>
        /// Является ли пользователь администратором true|false 
        /// </summary>
        public bool isAdmin
        {
            get { return crewnum == MiscConst.adminCrewNum; }
        }

        /// <summary>
        /// Совершен ли вход true|false
        /// </summary>
        public bool isLogged
        {
            get { return islogged; }
        }

        /// <summary>
        /// Получить ссылку на вошедшего пользователя
        /// </summary>
        public Partner loggedUser
        {
            get { return user; }
        }




    }
}