﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MVCSharp.Core.Configuration.Views;
using MobileRegister.UI.UserControls;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Mobile;
using MobileRegister.UI.Presenters;
using MobileRegister.UI.Properties;
using Bornander.UI;

namespace MobileRegister.UI.Views
{
    [View(typeof(MainTask), MainTask.Payment)]
    public partial class PaymentView : MobileFormViewForPaymentController, IPaymentView

    {
        private ImageButton btnPrint;
        private ImageButton btnCancel;
        private ImageButton btnCurrency;
        private SmoothListbox smLBox = new SmoothListbox();
        private PaymentLine lastAdded;
        public PaymentView()
        {   

            InitializeComponent();
            btnPrint = new ImageButton(Resources.buttonPrintReceipt, Resources.buttonPrintReceiptPressed);
            btnPrint.Location = new System.Drawing.Point(82, 279);
            btnPrint.Size = new System.Drawing.Size(156, 38);
            this.Controls.Add(btnPrint);

            btnCancel = new ImageButton(Resources.buttonCancel, Resources.buttonCancelPressed);
            btnCancel.Location = new System.Drawing.Point(1, 279);
            btnCancel.Size = new System.Drawing.Size(76, 38);
            this.Controls.Add(btnCancel);

            btnCurrency = new ImageButton(Resources.buttonEmptyRed, Resources.buttonEmptyRedPressed);
            btnCurrency.Location = new System.Drawing.Point(160, 27);
            btnCurrency.Size = new System.Drawing.Size(76, 38);
            panelTop.Controls.Add(btnCurrency);
            btnCurrency.DrawText = true;
            btnCurrency.ForeColor = Color.White;
            btnCurrency.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            btnCurrency.Text = "EUR";
            btnCurrency.BringToFront();


            smLBox.Dock = DockStyle.Fill;
            smLBox.BackColor = Color.LightSlateGray;

            panelMiddle.Controls.Add(smLBox);
            smLBox.Reset();
            this.KeyPreview = true;

            this.btnCancel.Click += new EventHandler(btnCancel_Click);
            this.btnPrint.Click += new EventHandler(btnPrint_Click);

            this.btnCurrency.Click += new EventHandler(btnCurChange_Click);
            this.Activated += new EventHandler(PaymentView_Activated);

            this.KeyPress += new KeyPressEventHandler(PaymentView_KeyPress);
            this.KeyDown += new KeyEventHandler(PaymentView_KeyDown);
        }


        #region IPaymentView impementation
        public event EventHandler<EventArgs> CancelButtonPressed;

        public event EventHandler<EventArgs> PrintButtonPressed;

        public event EventHandler<EventArgs> CurChangeButtonPressed;

        public event EventHandler<EventArgs> EnterButtonPressed;

        public event EventHandler<EventArgs> DigitButtonPressed;

        public event EventHandler<EventArgs> BackspaceButtonPressed;

        public event EventHandler<EventArgs> PaymentViewActivated;

        public event EventHandler<EventArgs> ItemDeleteEvent;

        public event EventHandler<EventArgs> PaymentLinesChanged;



        public string TotalToPay
        {
            set { lblTotal.Text = value; }
        }
        //******************************************
        public string ChangeRestAmount
        {
            set { lblChangeRestAmount.Text = value; }
        }

        public string ChangeRestName
        {
            set { lblChangeRestName.Text = value; }
        }
        //*******************************************

        public string PaidAmount
        {
            set { lblPaidAmount.Text = value; }
        }

        public string CurrentPayment
        {
            set { lblCurrentPayment.Text = value; }
        }


        public string CurrencyTotal
        {
            set {
                    lblCurName.Text = value;
                    btnCurrency.Text = value;
                }
        }

        #endregion

        void PaymentView_Activated(object sender, EventArgs e)
        {
            if (PaymentViewActivated != null)
                PaymentViewActivated(this, EventArgs.Empty);
        }

        void btnCurChange_Click(object sender, EventArgs e)
        {
            if (CurChangeButtonPressed != null)
                CurChangeButtonPressed(this, EventArgs.Empty);
        }

        void btnPrint_Click(object sender, EventArgs e)
        {
            if (PrintButtonPressed != null)
                PrintButtonPressed(this, EventArgs.Empty);
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelButtonPressed != null)
                CancelButtonPressed(this, EventArgs.Empty);
        }

        void PaymentView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (EnterButtonPressed != null)
                    EnterButtonPressed(this, EventArgs.Empty);
                e.Handled = true;
            }
        }

        void PaymentView_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((Char.IsDigit(e.KeyChar)) | (e.KeyChar=='.'))
            {
                if (DigitButtonPressed != null)
                    DigitButtonPressed(this, new DigitButtonEventArgs(e.KeyChar));
            }
            else
                if (e.KeyChar == (char)Keys.Back)
                {
                    if (BackspaceButtonPressed != null)
                        BackspaceButtonPressed(this, e);
                }
        }

        #region PaymentLine manipulation methods
        private PaymentLine FindPaymentLine(string curname )
        {
            PaymentLine tmpPaymentLine = null;
            foreach (Control tmpControl in smLBox.Items)
            {
                if (((PaymentLine)tmpControl).CurrencyName == curname)
                {
                    tmpPaymentLine = (PaymentLine)tmpControl;
                    return tmpPaymentLine;
                }
            }
            return tmpPaymentLine;
        }

        void delButtonPressed(object sender, EventArgs e)
        {
            if (ItemDeleteEvent != null)
                ItemDeleteEvent(this, e);
        }

        public void DeletePaymentLine()
        {
            smLBox.RemoveItem(smLBox.SelectedItems[0]);
            this.Focus();//strange workaround!
            if (PaymentLinesChanged != null)
                PaymentLinesChanged(this, EventArgs.Empty);
        }

        public void AddPaymentLine(string[] param)
        {
            var tmpPaymentLine = FindPaymentLine(param[1]);
            if (tmpPaymentLine != null)
            {
                lastAdded = tmpPaymentLine;
                lastAdded.SumLocal = lastAdded.SumLocal + Double.Parse(param[0]);
            }

            else
            {
                lastAdded = new PaymentLine(Double.Parse(param[0]),param[1], param[2]);//сменить на нормальную инициализацию
                this.smLBox.AddItem(lastAdded);
                lastAdded.delButtonPressed += new EventHandler<EventArgs>(delButtonPressed);
            
            lastAdded.Focus();

            if (PaymentLinesChanged != null)
                PaymentLinesChanged(this, EventArgs.Empty);
            }
        }


        public void ClearPaymentLines()
        {
            foreach (Control c in smLBox.Items)
                smLBox.RemoveItem(c);
            this.Focus();//strange workaround!
            if (PaymentLinesChanged != null)
                PaymentLinesChanged(this, EventArgs.Empty);
        }

        #endregion





    }
    public class MobileFormViewForPaymentController : MobileFormView<PaymentController> { }
}