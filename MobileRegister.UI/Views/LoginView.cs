﻿using System;
using System.Windows.Forms;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Core.Configuration.Views;
using MVCSharp.Mobile;
using MobileRegister.UI.Presenters;
using MobileRegister.UI.Properties;
using MobileRegister.UI.UserControls;

namespace MobileRegister.UI.Views
{
   
    [View(typeof(MainTask), MainTask.Login)]
    public partial class LoginView : MobileFormViewForLoginController, ILoginView
    {
        #region Screen buttons
        private ImageButton btn0;
        private ImageButton btn1;
        private ImageButton btn2;
        private ImageButton btn3;
        private ImageButton btn4;
        private ImageButton btn5;
        private ImageButton btn6;
        private ImageButton btn7;
        private ImageButton btn8;
        private ImageButton btn9;
        private ImageButton btnOk;
        private ImageButton btnCancel;

        #endregion

        public LoginView()
        {
            InitializeComponent();

            #region Screen buttons размещение

            this.SuspendLayout();

            btn1 = new ImageButton(Resources.buttonOne, Resources.buttonOnePressed);
            btn1.Location = new System.Drawing.Point(1, 101);
            btn1.Size = new System.Drawing.Size(76, 38);
            btn1.Text = "1";
            this.Controls.Add(btn1);

            btn2 = new ImageButton(Resources.buttonTwo, Resources.buttonTwoPressed);
            btn2.Location = new System.Drawing.Point(82, 101);
            btn2.Size = new System.Drawing.Size(76, 38);
            btn2.Text = "2";
            this.Controls.Add(btn2);

            btn3 = new ImageButton(Resources.buttonThree, Resources.buttonThreePressed);
            btn3.Location = new System.Drawing.Point(163, 101);
            btn3.Size = new System.Drawing.Size(76, 38);
            btn3.Text = "3";
            this.Controls.Add(btn3);

            btn4 = new ImageButton(Resources.buttonFour, Resources.buttonFourPressed);
            btn4.Location = new System.Drawing.Point(1, 145);
            btn4.Size = new System.Drawing.Size(76, 38);
            btn4.Text = "4";
            this.Controls.Add(btn4);

            btn5 = new ImageButton(Resources.buttonFive, Resources.buttonFivePressed);
            btn5.Location = new System.Drawing.Point(82, 145);
            btn5.Size = new System.Drawing.Size(76, 38);
            btn5.Text = "5";
            this.Controls.Add(btn5);

            btn6 = new ImageButton(Resources.buttonSix, Resources.buttonSixPressed);
            btn6.Location = new System.Drawing.Point(163, 145);
            btn6.Size = new System.Drawing.Size(76, 38);
            btn6.Text = "6";
            this.Controls.Add(btn6);

            btn7 = new ImageButton(Resources.buttonSeven, Resources.buttonSevenPressed);
            btn7.Location = new System.Drawing.Point(1, 189);
            btn7.Size = new System.Drawing.Size(76, 38);
            btn7.Text = "7";
            this.Controls.Add(btn7);

            btn8 = new ImageButton(Resources.buttonEight, Resources.buttonEightPressed);
            btn8.Location = new System.Drawing.Point(82, 189);
            btn8.Size = new System.Drawing.Size(76, 38);
            btn8.Text = "8";
            this.Controls.Add(btn8);

            btn9 = new ImageButton(Resources.buttonNine, Resources.buttonNinePressed);
            btn9.Location = new System.Drawing.Point(163, 189);
            btn9.Size = new System.Drawing.Size(76, 38);
            btn9.Text = "9";
            this.Controls.Add(btn9);

            btn0 = new ImageButton(Resources.buttonZero, Resources.buttonZeroPressed);
            btn0.Location = new System.Drawing.Point(82, 233);
            btn0.Size = new System.Drawing.Size(76, 38);
            btn0.Text = "0";
            this.Controls.Add(btn0);

            btnOk = new ImageButton(Resources.buttonInput, Resources.buttonInputPressed);
            btnOk.Location = new System.Drawing.Point(82, 279);
            btnOk.Size = new System.Drawing.Size(156, 38);
            //btnOk.Text = "9";
            this.Controls.Add(btnOk);

            btnCancel = new ImageButton(Resources.buttonCancel, Resources.buttonCancelPressed);
            btnCancel.Location = new System.Drawing.Point(1, 279);
            btnCancel.Size = new System.Drawing.Size(76, 38);
            //btnCancel.Text = "9";
            this.Controls.Add(btnCancel);


            this.ResumeLayout();

            #endregion

            this.btn0.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn1.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn2.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn3.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn4.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn5.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn6.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn7.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn8.Click += new System.EventHandler(this.buttonDigitClick);
            this.btn9.Click += new System.EventHandler(this.buttonDigitClick);
            this.btnOk.Click += new System.EventHandler(this.buttonInputPressed);
            this.btnCancel.Click += new System.EventHandler(this.buttonCancelClick);
            this.Load += new EventHandler(LoginView_Activated);

            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(LoginView_KeyDown);
            this.KeyPress += new KeyPressEventHandler(LoginView_KeyPress);
        }

        void LoginView_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (Char.IsDigit(e.KeyChar))
            {
                if (DigitButtonPressed != null)
                    DigitButtonPressed(this, new DigitButtonEventArgs(e.KeyChar));
            }
            //else
            //if ((e.KeyChar == (char)Keys.Back  )||(e.KeyChar == (char)Keys.Left  ))
            //{
            //    if (CancelButtonPressed != null)
            //        CancelButtonPressed(this, EventArgs.Empty);
            //}
        }

        void LoginView_KeyDown(object sender, KeyEventArgs e)
        {

            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Right))
            {
                if (InputButtonPressed != null)
                    InputButtonPressed(this, EventArgs.Empty);
                e.Handled = true;
            }
            else
            if ((e.KeyCode == Keys.Back) || (e.KeyCode == Keys.Left))
            {
                if (CancelButtonPressed != null)
                    CancelButtonPressed(this, EventArgs.Empty);
                e.Handled = true;
            }

        }



        public string StaffNumber
        {
            set { lblCrewNmb.Text = value; }
        }

        #region реализация интерфейса IView

        public event EventHandler<EventArgs> DigitButtonPressed;

        public event EventHandler<EventArgs> InputButtonPressed;

        public event EventHandler<EventArgs> CancelButtonPressed;

        public event EventHandler<EventArgs> LoginViewActivated;

        #endregion


        void LoginView_Activated(object sender, EventArgs e)
        {
            if (LoginViewActivated != null)
                LoginViewActivated(this, EventArgs.Empty);
        }



        private void buttonInputPressed(object sender, EventArgs args)
        {
            if (InputButtonPressed != null)
                InputButtonPressed(this, EventArgs.Empty);
 
        }

        private void buttonDigitClick(object sender, EventArgs args)
        {
            try
            {
                char tmpChar = Convert.ToChar(((Control)sender).Text);
                if (DigitButtonPressed != null)
                    DigitButtonPressed(this, new DigitButtonEventArgs(tmpChar));
            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void buttonCancelClick(object sender, EventArgs args)
        {
            if (CancelButtonPressed != null)
                CancelButtonPressed(this, EventArgs.Empty);

        }

        public void AppClose()
        {//пока стандартный диалог, потом надо сделать свой
            
            if (MessageBox.Show("Закрыть?","Вопрос",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)==
                DialogResult.Yes)
            Application.Exit();
        }


       

    }
    public class MobileFormViewForLoginController : MobileFormView<LoginController> { }
}