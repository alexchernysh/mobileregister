﻿using System;
using System.Windows.Forms;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Core.Configuration.Views;
using MVCSharp.Mobile;
using MobileRegister.UI.Presenters;
using MobileRegister.UI.Properties;
using MobileRegister.UI.UserControls;

namespace MobileRegister.UI.Views
{
    [View(typeof(MainTask), MainTask.MainMenu)]
    public partial class MainMenuView : MobileFormViewForMenuController, IMainMenuView
    {
        private ImageButton btnSale;
        private ImageButton btnReport;
        private ImageButton btnLoad;
        private ImageButton btnUnload;
        private ImageButton btnExit;


        public MainMenuView()
        {
            InitializeComponent();

            btnSale = new ImageButton(Resources.buttonSale, Resources.buttonSalePressed);
            btnSale.Location = new System.Drawing.Point(16, 28);
            btnSale.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnSale);

            btnReport = new ImageButton(Resources.buttonReports, Resources.buttonReportsPressed);
            btnReport.Location = new System.Drawing.Point(16, 82);
            btnReport.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnReport);

            btnLoad = new ImageButton(Resources.buttonDownloadData, Resources.buttonDownloadDataPressed, Resources.buttonDownloadDataDisabled);
            btnLoad.Location = new System.Drawing.Point(16, 136);
            btnLoad.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnLoad);

            btnUnload = new ImageButton(Resources.buttonUploadData, Resources.buttonUploadDataPressed, Resources.buttonUploadDataDisabled);
            btnUnload.Location = new System.Drawing.Point(16, 190);
            btnUnload.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnUnload);

            btnExit = new ImageButton(Resources.buttonExit, Resources.buttonExitPressed);
            btnExit.Location = new System.Drawing.Point(16, 244);
            btnExit.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnExit);


            btnExit.Enabled = true;
            btnLoad.Enabled = true;
            btnReport.Enabled = true;
            btnSale.Enabled = true;
            btnUnload.Enabled = true;

            this.Activated += new EventHandler(OnMenuViewActivated);
            this.btnSale.Click += new EventHandler(OnSaleButtonPressed);
            this.btnExit.Click += new EventHandler(OnExitButtonPressed);
            this.btnReport.Click += new EventHandler(OnReportsButtonPressed);
            this.btnLoad.Click += new EventHandler(OnLoadButtonPressed);
            this.btnUnload.Click += new EventHandler(OnUnloadButtonPressed);

        }
        #region IMenuView implementation
        public event EventHandler<EventArgs> SaleButtonPressed;
        public event EventHandler<EventArgs> ReportsButtonPressed;
        public event EventHandler<EventArgs> LoadButtonPressed;
        public event EventHandler<EventArgs> UnloadButtonPressed;
        public event EventHandler<EventArgs> ExitButtonPressed;
        public event EventHandler<EventArgs> MenuViewActivated;
        #endregion
        #region Event Handlers
        public void OnMenuViewActivated(object sender, EventArgs e)
        {
            if (MenuViewActivated != null)
                MenuViewActivated(this, EventArgs.Empty);
        }

        public void OnSaleButtonPressed(object sender, EventArgs e)
        {
            if (SaleButtonPressed != null)
                SaleButtonPressed(this, EventArgs.Empty);
        }

        public void OnReportsButtonPressed(object sender, EventArgs e)
        {
            if (ReportsButtonPressed != null)
                ReportsButtonPressed(this, EventArgs.Empty);
        }

        public void OnLoadButtonPressed(object sender, EventArgs e)
        {
            if (LoadButtonPressed != null)
                LoadButtonPressed(this, EventArgs.Empty);
        }

        public void OnUnloadButtonPressed(object sender, EventArgs e)
        {
            if (UnloadButtonPressed != null)
                UnloadButtonPressed(this, EventArgs.Empty);
        }

        public void OnExitButtonPressed(object sender, EventArgs e)
        {
            if (ExitButtonPressed != null)
                ExitButtonPressed(this, EventArgs.Empty);
        }

        #endregion
        #region Enable\Disable menu buttons
        public void AdminRights()
        {
            btnExit.Enabled = true;
            btnLoad.Enabled = true;
            btnReport.Enabled = true;
            btnSale.Enabled = true;
            btnUnload.Enabled = true;
        }
        public void UserRights()
        {
            btnExit.Enabled = true;
            btnLoad.Enabled = false;
            btnReport.Enabled = true;
            btnSale.Enabled = true;
            btnUnload.Enabled = false;
        }
        #endregion
    }
    public class MobileFormViewForMenuController : MobileFormView<MainMenuController> { }

}