﻿namespace MobileRegister.UI.Views
{
    partial class PaymentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblCurrentPayment = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCurName = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.panelMiddle = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblChangeRestAmount = new System.Windows.Forms.Label();
            this.lblChangeRestName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPaidAmount = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.lblCurrentPayment);
            this.panelTop.Controls.Add(this.label6);
            this.panelTop.Controls.Add(this.lblCurName);
            this.panelTop.Controls.Add(this.lblTotal);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(240, 69);
            // 
            // lblCurrentPayment
            // 
            this.lblCurrentPayment.BackColor = System.Drawing.Color.White;
            this.lblCurrentPayment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblCurrentPayment.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblCurrentPayment.ForeColor = System.Drawing.Color.Black;
            this.lblCurrentPayment.Location = new System.Drawing.Point(0, 42);
            this.lblCurrentPayment.Name = "lblCurrentPayment";
            this.lblCurrentPayment.Size = new System.Drawing.Size(240, 27);
            this.lblCurrentPayment.Text = "+";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(1, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 22);
            this.label6.Text = "К ОПЛАТЕ:";
            // 
            // lblCurName
            // 
            this.lblCurName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblCurName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblCurName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblCurName.Location = new System.Drawing.Point(112, 2);
            this.lblCurName.Name = "lblCurName";
            this.lblCurName.Size = new System.Drawing.Size(34, 22);
            this.lblCurName.Text = "<CUR>";
            this.lblCurName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Yellow;
            this.lblTotal.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTotal.Location = new System.Drawing.Point(147, 2);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(90, 22);
            this.lblTotal.Text = "<Total>";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelMiddle
            // 
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMiddle.Location = new System.Drawing.Point(0, 69);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(240, 145);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(112, 240);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 22);
            this.label1.Text = "EUR";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblChangeRestAmount
            // 
            this.lblChangeRestAmount.BackColor = System.Drawing.Color.Yellow;
            this.lblChangeRestAmount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.lblChangeRestAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblChangeRestAmount.Location = new System.Drawing.Point(147, 240);
            this.lblChangeRestAmount.Name = "lblChangeRestAmount";
            this.lblChangeRestAmount.Size = new System.Drawing.Size(90, 22);
            this.lblChangeRestAmount.Text = "<Total>";
            this.lblChangeRestAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblChangeRestName
            // 
            this.lblChangeRestName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblChangeRestName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblChangeRestName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblChangeRestName.Location = new System.Drawing.Point(1, 240);
            this.lblChangeRestName.Name = "lblChangeRestName";
            this.lblChangeRestName.Size = new System.Drawing.Size(110, 22);
            this.lblChangeRestName.Text = "<CHNG>";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(1, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 22);
            this.label5.Text = "ОПЛАЧЕНО:";
            // 
            // lblPaidAmount
            // 
            this.lblPaidAmount.BackColor = System.Drawing.Color.Yellow;
            this.lblPaidAmount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.lblPaidAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPaidAmount.Location = new System.Drawing.Point(147, 217);
            this.lblPaidAmount.Name = "lblPaidAmount";
            this.lblPaidAmount.Size = new System.Drawing.Size(90, 22);
            this.lblPaidAmount.Text = "<Total>";
            this.lblPaidAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(112, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 22);
            this.label8.Text = "EUR";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PaymentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPaidAmount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblChangeRestAmount);
            this.Controls.Add(this.lblChangeRestName);
            this.Controls.Add(this.panelMiddle);
            this.Controls.Add(this.panelTop);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "PaymentView";
            this.Text = "PaymentView";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelMiddle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblChangeRestAmount;
        private System.Windows.Forms.Label lblChangeRestName;
        private System.Windows.Forms.Label lblCurName;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCurrentPayment;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPaidAmount;
        private System.Windows.Forms.Label label8;
    }
}