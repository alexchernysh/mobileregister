﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MVCSharp.Core.Configuration.Views;
using MobileRegister.UI.UserControls;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Mobile;
using MobileRegister.UI.Presenters;
using MobileRegister.UI.Properties;
using Bornander.UI;
using Bluebird.Barcode;
using Microsoft.WindowsCE.Forms;
using System.Runtime.InteropServices;



namespace MobileRegister.UI.Views
{
   
    [View(typeof(MainTask), MainTask.Sale)]
    public partial class SaleView : MobileFormViewForSaleController, ISaleView
    {
        public static Barcode m_Barcode = new Barcode();
        MsgHandler m_MsgHandler;

        private bool TryInitBarcodeRdr = true;

        private ImageButton btnExit;
        private ImageButton btnCancelReceipt;
        private ImageButton btnPayment;
        private ImageButton btnCurrency;
        private ImageButton btnDiscount;
        private ImageButton btnSale_Return;

        private SmoothListbox smLBox = new SmoothListbox();
        private ItemLine lastAdded;
        public SaleView()
        {
            InitializeComponent();
            m_MsgHandler = new MsgHandler(this);

            #region ImageButtons

            this.SuspendLayout();

            btnExit = new ImageButton(Resources.buttonSaleExit, Resources.buttonSaleExitPressed);
            btnExit.Location = new System.Drawing.Point(3, 67);
            btnExit.Size = new System.Drawing.Size(54, 38);
            this.panelBottom.Controls.Add(btnExit);


            btnCancelReceipt = new ImageButton(Resources.buttonReceiptCancel, Resources.buttonReceiptCancelPressed);
            btnCancelReceipt.Location = new System.Drawing.Point(3, 35);
            btnCancelReceipt.Size = new System.Drawing.Size(114, 26);
            this.panelBottom.Controls.Add(btnCancelReceipt);

            btnPayment = new ImageButton(Resources.buttonPayment, Resources.buttonPaymentPressed);
            btnPayment.Location = new System.Drawing.Point(122, 67);
            btnPayment.Size = new System.Drawing.Size(116, 38);
            this.panelBottom.Controls.Add(btnPayment);

            btnSale_Return = new ImageButton(Resources.buttonSale_Return, Resources.buttonSale_ReturnPressed);
            btnSale_Return.Location = new System.Drawing.Point(63, 67);
            btnSale_Return.Size = new System.Drawing.Size(54, 38);
            this.panelBottom.Controls.Add(btnSale_Return);

            //btnCurrency = new ImageButton(Resources.buttonCurrency, Resources.buttonCurrencyPressed);
            //btnCurrency.Location = new System.Drawing.Point(122, 35);
            //btnCurrency.Size = new System.Drawing.Size(55, 26);
            //this.panelBottom.Controls.Add(btnCurrency);

            //btnDiscount = new ImageButton(Resources.buttonDiscount, Resources.buttonDiscountPressed);
            //btnDiscount.Location = new System.Drawing.Point(183, 35);
            //btnDiscount.Size = new System.Drawing.Size(55, 26);
            //this.panelBottom.Controls.Add(btnDiscount);

            

            #endregion
            
            smLBox.Dock = DockStyle.Fill;
            smLBox.BackColor = Color.LightSlateGray;
            //smLBox.UnselectEnabled = true;
            panelReceipt.Controls.Add(smLBox);
            smLBox.Reset();

            this.ResumeLayout();
            this.KeyPreview = true;

            this.btnExit.Click += new EventHandler(buttonExitPressed);
            this.btnCancelReceipt.Click += new EventHandler(btnCancelReceipt_Click);

            this.btnPayment.Click += new EventHandler(btnPayment_Click);

            
            this.KeyPress += new KeyPressEventHandler(SaleView_KeyPress);
            this.KeyDown += new KeyEventHandler(SaleView_KeyDown);
            this.Activated += new EventHandler(SaleView_Activated);
        }


        #region barcode related staff
        public class MsgHandler : MessageWindow
        {
            public const int WM_USER = 0x0400;
            public const int WM_SCANTRIGGER = WM_USER + 702;

            private SaleView m_myForm;

            public MsgHandler(SaleView form)
            {
                m_myForm = form;
            }
            // It is receive window message and get barcode data
            protected override void WndProc(ref Message msg)
            {
                switch (msg.Msg)
                {
                    case WM_SCANTRIGGER:
                        this.m_myForm.GetBarcodeData();
                        break;
                }
            }
        }

        [DllImport("CoreDll.DLL", EntryPoint = "PlaySound", SetLastError = true)]
        private extern static int WCE_playsounduse(string szSound, IntPtr hMod, int flags);


        public void GetBarcodeData()
        {

            byte[] uBuf = new byte[4096];

            int nBufSize = 4096;
            int nReadSize = 4096;
            byte type = new byte();

            //If there is NULL in Barcode data, use BBBarcodeGetDecodeDataNTypeRaw() function
            /*if (0 == m_Barcode.GetDecodeDataNTypeRaw(uBuf, ref type, nBufSize, ref nReadSize))
            {
                txtData.Text = Encoding.UTF8.GetString(uBuf, 0, uBuf.Length).Replace("\0", "").Trim();
                txtType.Text = GetBarcodeType(type);
            }*/

            StringBuilder buf = new StringBuilder(1024);

            if (m_Barcode.GetDecodeDataNType(buf, ref type, nBufSize, ref nReadSize))
            {
                WCE_playsounduse("\\ProgramStore\\data\\barcode.wav", IntPtr.Zero, 0x00020001);

                foreach (char ch in buf.ToString())
                {
                    DigitButtonPressed(this, new DigitButtonEventArgs(ch));
                }
                SaleView_KeyDown(this, new KeyEventArgs(Keys.Enter));
            }
        }

        #endregion        

        #region public properties

        public string ItemCode
        {
            set { lblItemCode.Text = value; }
        }

        public string Info
        {
            set
            {
                //lblInfo.Visible = value != String.Empty;
                if (value != String.Empty)
                {
                    lblInfo.BackColor = Color.Yellow;
                }
                else
                {
                    lblInfo.BackColor = Color.White;
                }
                lblInfo.Text = value;
            }
                
        }

        public string Total
        {
            set { lblTotal.Text = value; }
        }

        public string CurName
        {
            set { lblCurName.Text = value; }
        }
        #endregion

        #region Events & Event handlers
        public event EventHandler<EventArgs> ItemLinesChanged;

        public event EventHandler<EventArgs> EnterButtonPressed;

        public event EventHandler<EventArgs> ExitButtonPressed;

        public event EventHandler<EventArgs> DigitButtonPressed;

        public event EventHandler<EventArgs> BackspaceButtonPressed;

        public event EventHandler<EventArgs> PaymentButtonPressed;

        public event EventHandler<EventArgs> ReceiptCancelButtonPressed;

        public event EventHandler<EventArgs> SaleViewActivated;

        public event EventHandler<EventArgs> ItemPlusEvent;

        public event EventHandler<EventArgs> ItemMinusEvent;

        public event EventHandler<EventArgs> ItemDeleteEvent;


        void SaleView_Activated(object sender, EventArgs e)
        {
            if (TryInitBarcodeRdr)
            {
                #region barcode related staff
                try
                {
                    if (!m_Barcode.Open(true))
                    {
                        MessageBox.Show("Barcode Open Fail");
                    }
                    else
                    {
                        if (!m_Barcode.SetClientHandle(m_MsgHandler.Hwnd)) // Set window handle that receive to message
                        {
                            MessageBox.Show("Set Client Error");
                        }
                    }
                    m_Barcode.SetVirtualWedge(false);
                }
                catch (Exception ex)
                {
                    TryInitBarcodeRdr = false;
                    //MessageBox.Show("Бибилиотека сканирования не инициализирована " + ex.Message);
                }
                #endregion
            }

            if (SaleViewActivated != null)
                    SaleViewActivated(this, EventArgs.Empty);   
        }

        void btnPayment_Click(object sender, EventArgs e)
        {
            if (PaymentButtonPressed != null)
                PaymentButtonPressed(this, EventArgs.Empty);
        }

        void btnCancelReceipt_Click(object sender, EventArgs e)
        {
            if (ReceiptCancelButtonPressed != null)
                ReceiptCancelButtonPressed(this, EventArgs.Empty);
        }
        
        void SaleView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (EnterButtonPressed != null)
                    EnterButtonPressed(this, EventArgs.Empty);
                e.Handled = true;
            }
        }

        void SaleView_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (Char.IsDigit(e.KeyChar))
            {
                if (DigitButtonPressed != null)
                    DigitButtonPressed(this, new DigitButtonEventArgs(e.KeyChar));
            }
            else
                if (e.KeyChar == (char)Keys.Back)
                {
                    if (BackspaceButtonPressed != null)
                        BackspaceButtonPressed(this, e);
                }
        }

        void buttonExitPressed(object sender, EventArgs args)
        {
            //освободить handle и выключить чтение штрихкода
            m_Barcode.ReleaseClientHandle();
            m_Barcode.Close();


            if (ExitButtonPressed != null)
                ExitButtonPressed(this, EventArgs.Empty);
        }

        #endregion

        #region ItemLines manipulation methods

        private ItemLine FindItemLine(string plu)
        {
            ItemLine tmpItemLine = null;
            foreach (Control tmpControl in smLBox.Items)
            {
                if (((ItemLine)tmpControl).ItemPlu == plu)
                {
                    tmpItemLine = (ItemLine)tmpControl;
                    return tmpItemLine;
                }
            }
            return tmpItemLine;
        }

        void delButtonPressed(object sender, EventArgs e)
        {
            if (ItemDeleteEvent != null)
                ItemDeleteEvent(this, e);          
        }

        void minusButtonPressed(object sender, EventArgs e)
        {
            if (ItemMinusEvent != null)
                ItemMinusEvent(this, e);
        }

        void plusButtonPressed(object sender, EventArgs e)
        {
            if (ItemPlusEvent != null)
                ItemPlusEvent(this, e);
        }

        public void UpdateItemLine(int qty)
        {

        }

        public void DeleteItemLine()
        {
           smLBox.RemoveItem( smLBox.SelectedItems[0]);
           this.Focus();//strange workaround!
           if (ItemLinesChanged != null)
               ItemLinesChanged(this, EventArgs.Empty);
        }

        public void AddItemLine(string[] param)
        {
            var tmpItemLine = FindItemLine(param[3]);
            if (tmpItemLine != null)
            {
                lastAdded = tmpItemLine;
                lastAdded.Qty = lastAdded.Qty + 1;
            }
            else
            {
                lastAdded = new ItemLine(param[3] , param[0], param[1], param[2]);
                this.smLBox.AddItem(lastAdded);
                lastAdded.plusButtonPressed += new EventHandler<EventArgs>(plusButtonPressed);
                lastAdded.minusButtonPressed += new EventHandler<EventArgs>(minusButtonPressed);
                lastAdded.delButtonPressed += new EventHandler<EventArgs>(delButtonPressed);
            }
            lastAdded.Focus();

            if (ItemLinesChanged != null)
                ItemLinesChanged(this, EventArgs.Empty);
        }


        public void ClearItemLines()
        {
            foreach (Control c in smLBox.Items)
            smLBox.RemoveItem(c);
            this.Focus();//strange workaround!
            if (ItemLinesChanged != null)
                ItemLinesChanged(this, EventArgs.Empty);
        }
        #endregion

    }
    public class MobileFormViewForSaleController : MobileFormView<SaleController> { }


}