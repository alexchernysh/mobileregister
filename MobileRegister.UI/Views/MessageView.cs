﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MVCSharp.Core.Configuration.Views;
using MobileRegister.UI.UserControls;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Mobile;
using MobileRegister.UI.Properties;
using MobileRegister.UI.Presenters;

namespace MobileRegister.UI.Views
{
    [View(typeof(MainTask), MainTask.Message)]
    public partial class MessageView : MobileFormViewForMessageController,IMessageView
    {
        private ImageButton btnOk;
        public MessageView()
        {
            InitializeComponent();

            btnOk = new ImageButton(Resources.buttonOK, Resources.buttonOKPressed);
            btnOk.Location = new System.Drawing.Point(16, 244);
            btnOk.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnOk);

            btnOk.Click += new EventHandler(btnOk_Click);

            this.Activated += new EventHandler(OnMessageViewActivated);
            lblMessage.Text = String.Empty;
        }

        void OnMessageViewActivated(object sender, EventArgs e)
        {
            if (MessageViewActivated != null)
                MessageViewActivated(this, EventArgs.Empty);
        }

        void btnOk_Click(object sender, EventArgs e)
        {
            if (OkButtonPressed != null)
            {
                OkButtonPressed(this, EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs> OkButtonPressed;
        public event EventHandler<EventArgs> MessageViewActivated;

        public string message
        {
            set { lblMessage.Text = value; }
            get { return lblMessage.Text; }
        }
    }
    public class MobileFormViewForMessageController : MobileFormView<MessageController> { }
}



