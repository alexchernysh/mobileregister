﻿using System;
using System.Windows.Forms;
using MobileRegister.UI.Abstract;
using MobileRegister.UI.Misc;
using MVCSharp.Core.Configuration.Views;
using MVCSharp.Mobile;
using MobileRegister.UI.UserControls;
using MobileRegister.UI.Properties;
using MobileRegister.UI.Presenters;

namespace MobileRegister.UI.Views
{
    [View(typeof(MainTask), MainTask.LoadMenu)]
    public partial class LoadMenuView : MobileFormViewForLoadMenuController, ILoadMenuView
    {
        private ImageButton btnLoadNet;
        private ImageButton btnLoadLocal;
        private ImageButton btnBack;

        public LoadMenuView()
        {
            InitializeComponent();

            btnLoadNet = new ImageButton(Resources.buttonLoadNet, Resources.buttonLoadNetPressed);
            btnLoadNet.Location = new System.Drawing.Point(16, 28);
            btnLoadNet.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnLoadNet);

            btnLoadLocal = new ImageButton(Resources.buttonLoadLocal, Resources.buttonLoadLocalPressed);
            btnLoadLocal.Location = new System.Drawing.Point(16, 82);
            btnLoadLocal.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnLoadLocal);

            btnBack = new ImageButton(Resources.buttonBack, Resources.buttonBackPressed);
            btnBack.Location = new System.Drawing.Point(16, 244);
            btnBack.Size = new System.Drawing.Size(204, 48);
            this.Controls.Add(btnBack);

            this.btnBack.Click += new EventHandler(OnBackButtonPressed);
            this.btnLoadNet.Click += new EventHandler(OnLoadNetButtonPressed);
            this.btnLoadLocal.Click+=new EventHandler(btnLoadLocal_Click);

        }

        private string filename;

        public string fileName
        { get {return filename;} }

        public event EventHandler<EventArgs> BackButtonPressed;
        public event EventHandler<EventArgs> FileNameAssigned;
        public event EventHandler<EventArgs> LoadNetButtonPressed;

        private void btnLoadLocal_Click(object sender, EventArgs e)
        {
            if (openXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                Application.DoEvents();
                Cursor.Current = Cursors.WaitCursor;
                
                filename = openXMLFileDialog.FileName;
                if (FileNameAssigned != null)
                    FileNameAssigned(this, EventArgs.Empty);
                Cursor.Current = Cursors.Default;
            }
        }

        void OnBackButtonPressed(object sender, EventArgs e)
        {
            if (BackButtonPressed != null)
                BackButtonPressed(this, EventArgs.Empty);
        }

        void OnLoadNetButtonPressed(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor.Current = Cursors.WaitCursor;
            if (LoadNetButtonPressed != null)
                LoadNetButtonPressed(this, EventArgs.Empty);
            Cursor.Current = Cursors.Default;
        }
    }
    public class MobileFormViewForLoadMenuController : MobileFormView<LoadMenuController> { }

}