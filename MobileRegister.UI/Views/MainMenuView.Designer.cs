﻿namespace MobileRegister.UI.Views
{
    partial class MainMenuView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaleS = new System.Windows.Forms.Button();
            this.btnReportS = new System.Windows.Forms.Button();
            this.btnLoadS = new System.Windows.Forms.Button();
            this.btnUnloadS = new System.Windows.Forms.Button();
            this.btnExitS = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSaleS
            // 
            this.btnSaleS.Location = new System.Drawing.Point(16, 28);
            this.btnSaleS.Name = "btnSaleS";
            this.btnSaleS.Size = new System.Drawing.Size(204, 48);
            this.btnSaleS.TabIndex = 0;
            this.btnSaleS.Text = "Торговля";
            this.btnSaleS.Visible = false;
            // 
            // btnReportS
            // 
            this.btnReportS.Location = new System.Drawing.Point(16, 82);
            this.btnReportS.Name = "btnReportS";
            this.btnReportS.Size = new System.Drawing.Size(204, 48);
            this.btnReportS.TabIndex = 0;
            this.btnReportS.Text = "Отчеты";
            this.btnReportS.Visible = false;
            // 
            // btnLoadS
            // 
            this.btnLoadS.Location = new System.Drawing.Point(16, 136);
            this.btnLoadS.Name = "btnLoadS";
            this.btnLoadS.Size = new System.Drawing.Size(204, 48);
            this.btnLoadS.TabIndex = 0;
            this.btnLoadS.Text = "Загрузка данных";
            this.btnLoadS.Visible = false;
            // 
            // btnUnloadS
            // 
            this.btnUnloadS.Location = new System.Drawing.Point(16, 190);
            this.btnUnloadS.Name = "btnUnloadS";
            this.btnUnloadS.Size = new System.Drawing.Size(204, 48);
            this.btnUnloadS.TabIndex = 0;
            this.btnUnloadS.Text = "Выгрузка данных";
            this.btnUnloadS.Visible = false;
            // 
            // btnExitS
            // 
            this.btnExitS.Location = new System.Drawing.Point(16, 244);
            this.btnExitS.Name = "btnExitS";
            this.btnExitS.Size = new System.Drawing.Size(204, 48);
            this.btnExitS.TabIndex = 0;
            this.btnExitS.Text = "Выход";
            this.btnExitS.Visible = false;
            // 
            // MainMenuView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnExitS);
            this.Controls.Add(this.btnUnloadS);
            this.Controls.Add(this.btnLoadS);
            this.Controls.Add(this.btnReportS);
            this.Controls.Add(this.btnSaleS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "MainMenuView";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSaleS;
        private System.Windows.Forms.Button btnReportS;
        private System.Windows.Forms.Button btnLoadS;
        private System.Windows.Forms.Button btnUnloadS;
        private System.Windows.Forms.Button btnExitS;
    }
}