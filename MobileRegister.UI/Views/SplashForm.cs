﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MobileRegister.UI.Views
{
    public partial class SplashForm : Form
    {
        public SplashForm()
        {
            InitializeComponent();

        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            Timer timer = new Timer();
            timer.Interval = 3000;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Enabled = true;
           
        }

        void timer_Tick(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}