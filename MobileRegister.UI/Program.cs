﻿using System;
using System.Threading;

using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using MobileRegister.UI.Views;
using MobileRegister.UI.Models;
using MobileRegister.UI.Presenters;
using MobileRegister.UI.Abstract;
using MVCSharp.Core.Tasks;
using MobileRegister.UI.Misc;
using MVCSharp.Mobile;
using MobileRegister.Utility;

namespace MobileRegister.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            //SplashForm sfrm = new SplashForm();
            //sfrm.Show();
            //sfrm.Update();
            WinMenuSwitch.ShowTopStatusbar(false);
            Application.DoEvents();
            TasksManager tasksManager = new TasksManager(MobileformsViewsManager.GetDefaultConfig());

            ITask task = tasksManager.StartTask(typeof(MainTask));
            Application.Run(MobileformsViewsManager.LastActivatedForm);


            

        }
    }
}