﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileRegister.UI.Abstract
{
    public interface ILoadMenuView
    {
        
        string fileName
        {get;}
        event EventHandler<EventArgs> BackButtonPressed;
        event EventHandler<EventArgs> FileNameAssigned;
        event EventHandler<EventArgs> LoadNetButtonPressed;


    }
}
