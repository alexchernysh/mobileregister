﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileRegister.UI.Abstract
{
    public interface IMessageView
    {
        string message
        { get; set; }
        event EventHandler<EventArgs> OkButtonPressed;
        event EventHandler<EventArgs> MessageViewActivated;
    }
}
