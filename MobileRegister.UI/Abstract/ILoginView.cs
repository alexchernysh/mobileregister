﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace MobileRegister.UI.Abstract
{
    public interface ILoginView
    {

        string StaffNumber { set; }


        /// <summary>
        /// Событие нажатия кнопки "ввод"
        /// </summary>
        event EventHandler<EventArgs> InputButtonPressed;

        /// <summary>
        /// Событие нажатия кнопки "цифра"
        /// </summary>
        event EventHandler<EventArgs> DigitButtonPressed;


        /// <summary>
        /// Событие нажатия кнопки "отмена"
        /// </summary>
        event EventHandler<EventArgs> CancelButtonPressed;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler<EventArgs> LoginViewActivated;
        void AppClose();


    }
}
