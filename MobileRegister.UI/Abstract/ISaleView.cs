﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using MobileRegister.UI.UserControls;

namespace MobileRegister.UI.Abstract
{
    public interface ISaleView
    {
        string ItemCode { set; }
        string Info { set; }
        string Total { set; }
        string CurName { set; }
        //ICollection ItemLines { get; }

        ///// <summary>
        ///// Событие нажатия кнопки "+"
        ///// </summary>
        //event EventHandler<EventArgs> PlusButtonPressed;

        ///// <summary>
        ///// Событие нажатия кнопки "-"
        ///// </summary>
        //event EventHandler<EventArgs> MinusButtonPressed;


        /// <summary>
        /// 
        /// </summary>
        event EventHandler<EventArgs> ItemLinesChanged;


        /// <summary>
        /// Событие нажатия кнопки "продажа-возврат"
        /// </summary>
        event EventHandler<EventArgs> EnterButtonPressed;

        /// <summary>
        /// Событие нажатия кнопки "выход"
        /// </summary>
        event EventHandler<EventArgs> ExitButtonPressed;

        /// <summary>
        /// Событие нажатия кнопки на клавиатуре
        /// </summary>
        event EventHandler<EventArgs> DigitButtonPressed;

        event EventHandler<EventArgs> ItemDeleteEvent;

        event EventHandler<EventArgs> ItemMinusEvent;

        event EventHandler<EventArgs> ItemPlusEvent;

        event EventHandler<EventArgs> BackspaceButtonPressed;

        event EventHandler<EventArgs> PaymentButtonPressed;

        event EventHandler<EventArgs> SaleViewActivated;

        event EventHandler<EventArgs> ReceiptCancelButtonPressed;

        void AddItemLine(string[] param);

        void ClearItemLines();

        void DeleteItemLine();

        void UpdateItemLine(int qty);

        //void RefreshTotal();

        //void DeleteItemLine(string code);


    }
}
