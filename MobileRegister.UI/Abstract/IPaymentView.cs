﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using MobileRegister.UI.UserControls;

namespace MobileRegister.UI.Abstract
{
    interface IPaymentView
    {
        string TotalToPay { set; }

        string ChangeRestAmount { set; }

        string PaidAmount { set; }

        string ChangeRestName { set; }

        string CurrencyTotal { set; }
        //string CurrencyRest { set; }
        //string CurrencyChange { set; }
        string CurrentPayment { set; }



        event EventHandler<EventArgs> PaymentLinesChanged;

        event EventHandler<EventArgs> CancelButtonPressed;

        event EventHandler<EventArgs> PrintButtonPressed;

        event EventHandler<EventArgs> CurChangeButtonPressed;

        event EventHandler<EventArgs> PaymentViewActivated;

        event EventHandler<EventArgs> EnterButtonPressed;

        event EventHandler<EventArgs> DigitButtonPressed;

        event EventHandler<EventArgs> BackspaceButtonPressed;

        event EventHandler<EventArgs> ItemDeleteEvent;

        void AddPaymentLine(string[] param);
        void ClearPaymentLines();
    }
}
