﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileRegister.UI.Abstract
{
    public interface IMainMenuView
    {
        //bool IsAdmin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        event EventHandler<EventArgs> MenuViewActivated;
        event EventHandler<EventArgs> SaleButtonPressed;
        event EventHandler<EventArgs> ReportsButtonPressed;
        event EventHandler<EventArgs> LoadButtonPressed;
        event EventHandler<EventArgs> UnloadButtonPressed;
        event EventHandler<EventArgs> ExitButtonPressed;
        void AdminRights();
        void UserRights();
    }
}
