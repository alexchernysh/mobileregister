﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Collections;
using System.Text;


namespace MobileRegister.Domain.ConnectorLib
{
    class RespondParser
    {
        private ArrayList SetList = new ArrayList();
        private ArrayList RecList = new ArrayList();
        private ArrayList FldList = new ArrayList();

        public void Parse(StreamReader Strm)
        {
            string s = Strm.ReadToEnd();
            SetList.Clear();
            SetList.Add(s);
            if (s.IndexOf("####") != -1) 
            { 
                RecList.Add(s);
                FldList.Add(s);
            }
            
        }

        public int DoFieldList(int ARecNumber)
        {
          if (RecList.Count <= ARecNumber) 
          {
           throw new Exception("Номер записи слишком велик !");
          }
          FldList.Clear();
          ParseToList( Convert.ToString(RecList[ARecNumber]) , ":",  FldList);
          return FldList.Count;
        }

        public int DoFieldList()
        {
            return DoFieldList(0);
        }


        public int DoRecList(int ASetNumber)
        {
            if (SetList.Count <= ASetNumber) 
            { 
                throw new Exception("Номер набора слишком велик !");
            }
            RecList.Clear();
            ParseToList(Convert.ToString(SetList[0]), "\n" , RecList);
            return RecList.Count;
        }

        public int DoRecList()
        {
            return DoRecList(0);
        }



        public string GetRawString()
        {
            return Convert.ToString( SetList[0]);  
        }

        public string GetField(int AFldNumber)
        {         
            if (FldList.Count <= AFldNumber)     
            { 
                throw new Exception("Номер поля слишком велик !");
            }
             return  Convert.ToString(FldList[AFldNumber]);
           
        }

        public string GetField()
                {
                    return GetField(0);
                }

        public int FieldCount
        {
            get
            {
                return FldList.Count;
            }
        }


        public int RecCount
        {
            get
            {
                return RecList.Count;
            }
        }
        public int SetCount
        {
            get
            {
                return SetList.Count;
            }
        }



        public void ParseToList(String AInputString, String chrDvd, ArrayList AOutputList)
        {
            StringTokenizer tmpTokenizer = new StringTokenizer(AInputString, chrDvd);
            for (int i=0 ; i<=tmpTokenizer.CountTokens() - 1; i++)
            {
                AOutputList.Add(tmpTokenizer.NextToken());
            }

        }

    }
}

