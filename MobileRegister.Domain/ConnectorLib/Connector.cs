﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;


namespace MobileRegister.Domain.ConnectorLib
{
    public class Connector
    {
        public static string _url;
        private RequestComposer Composer = new RequestComposer();
        private RespondParser Parser = new RespondParser();

        public int DoRecList(int ASetNumber)
        {
            return Parser.DoRecList(ASetNumber);
        }

        public int DoRecList()
        {
            return Parser.DoRecList();
        }

        public int DoFieldList(int ARecNumber)
        {
            return Parser.DoFieldList(ARecNumber);
        }

        public int DoFieldList()
        {
            return Parser.DoFieldList();
        }

       

        public void Execute()
        {
            System.Text.Encoding encoding = Encoding.GetEncoding(1251);
            byte[] data = encoding.GetBytes(Composer.RequestString);
            //MessageBox.Show(Composer.RequestString);
            System.Uri _uri = new Uri(_url);
            HttpWebRequest _request = WebRequest.CreateDefault(_uri) as HttpWebRequest;
            _request.Timeout = 6000000;
            _request.Method = "POST";
            _request.ContentType = "application/x-www-form-urlencoded";
            _request.ContentLength = data.Length;
            //*******************************************************
            _request.Proxy = null;// GlobalProxySelection.GetEmptyWebProxy();
            //*******************************************************
            Stream tmpStream = _request.GetRequestStream();
            tmpStream.Write(data, 0, data.Length);
            tmpStream.Close();
            WebResponse _webResponse = _request.GetResponse();
            Stream receiveStream = _webResponse.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, encoding);

            Parser.Parse(readStream);
            readStream.Close();
            _webResponse.Close();

        }
        public void ExecuteAndParse()
        {
            this.Execute();
            this.DoRecList();
            this.DoFieldList();
        }
        public void Parse()
        {            
            this.DoRecList();
            this.DoFieldList();
        }
        public void AddParam(string AName, string AValue)
        {
            Composer.AddParam(AName, AValue);
        }
        public string GetRawString()
        {
            return Parser.GetRawString();
        }
        public string GetFieldAsString(int FieldNumber)
        {
            return Parser.GetField(FieldNumber);
        }
        public int FieldCount()
        {
            return Parser.FieldCount;
        }
        public int RecCount()
        {
            return Parser.RecCount;
        }
        public string URL
        {
            get
            {
                return  _url;
            }

            set
            {
                _url = value;
            }
            

        }
        public string Mode
        { 
            set
            {
                Composer.Mode =value;
            }
        }
    }
}

