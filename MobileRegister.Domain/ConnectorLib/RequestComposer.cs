﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace MobileRegister.Domain.ConnectorLib
{
    public class RequestComposer
    {        
        private ArrayList paramList=new ArrayList();
        private const string d_amp="&";
        private string _mode;         


        
        
        public string Mode
        {
            set
            {
               if (value.Length < 1) 
             {
                throw new Exception("Имя модуля не может быть пустым !");
             }
             _mode = value;
             paramList.Clear();
            }

        }
        public string RequestString 
        {
            get 
            {
                string res="";
                
                if (_mode.Length==0) 
                    {
                        throw new Exception("Имя модуля не может быть пустым !");
                    }
                res="operation"+"="+_mode+d_amp;
                for (int i=0; i< paramList.Count;i++)
                    {
                        res = res + Convert.ToString(paramList[i]) + d_amp;
                    }
                return res;
            }
        }
        
        public void AddParam(string AName, string AValue)
        {
            paramList.Add(AName + '=' + StringToServer(AValue));
        }

        public void ClearParam()
        { 
             paramList.Clear();
        }
        public string StringToServer(string S)
        {
            return S.Replace("+", "%2B");
        }       
    }   
}

 