﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace MobileRegister.Domain.ConnectorLib
{
    class StringTokenizer
    {   
        private ArrayList tokens;
        private string StrSource;
        private string StrDelimiter;
        private int NumTokens;
        private int CurrIndex;
        //default constructor
        public string Source
        {
            get
            {
                return this.StrSource;
            }
        }
        public string Delim
        { 
            get
            {
                return this.StrDelimiter;
            }
        }
        public StringTokenizer()
        {
            new StringTokenizer("","");
        }
        //2-nd constructor
        public StringTokenizer(string source)
        {
            new StringTokenizer(source, "");
        }
        //3-rd constructor
        public StringTokenizer(string source, string delimiter):
            base()
        {
            
            tokens= new ArrayList(10); 
           StrSource=source;
           StrDelimiter=delimiter;

           if (delimiter.Length == 0) 
           {
               StrDelimiter="";                
           }

           Tokenize();
        }
        //4-th constructor
        public StringTokenizer(string source, char[] delimiter)
        {
             new StringTokenizer(source,delimiter.ToString() );
        }

        private void Tokenize()
        {
            String TempSource=StrSource;
            String Tok="";
            NumTokens=0;
            tokens.Clear();
            CurrIndex=0;
            if ((TempSource.IndexOf(StrDelimiter) <0) & (TempSource.Length>0)) 
            {
                NumTokens=1;
                CurrIndex=0;
                tokens.Add(TempSource);
                tokens.TrimToSize();
            }
            else
            {
               if ((TempSource.IndexOf(StrDelimiter)<0) & (TempSource.Length<=0)) 
               {
                   NumTokens=0;
                   CurrIndex=0;
                   tokens.TrimToSize();
               }
            }
            while (TempSource.IndexOf(StrDelimiter)>=0) 
            {
                if (TempSource.IndexOf(StrDelimiter) == 0)
                {
                    if (TempSource.Length > StrDelimiter.Length)
                    {
                        TempSource = TempSource.Substring(StrDelimiter.Length);
                    }
                    else 
                    {
                        TempSource="";
                    }

                } 
                else
                {
                    Tok= TempSource.Substring(0, TempSource.IndexOf(this.StrDelimiter));
                    this.tokens.Add(Tok);
                    if (TempSource.Length > (this.StrDelimiter.Length + Tok.Length))
                    {
                        TempSource = TempSource.Substring((this.StrDelimiter.Length + Tok.Length));
                    }
                    else
                    {
                        TempSource = ""; 
                    }
                }
            
            }
            if (TempSource.Length > 0) 
            { 
                 this.tokens.Add(TempSource); 
            }

                 this.tokens.TrimToSize();
                 this.NumTokens = this.tokens.Count;
           
        }
        public void NewSource(string newSrc)
        {
            this.StrSource = newSrc;
            this.Tokenize();
        }
        public void NewDelim(string newDel)
        {
            if (newDel.Length == 0)     
                {
                    this.StrDelimiter =  " ";
                }
            else
                {
                 this.StrDelimiter = newDel;                 
                }
            this.Tokenize();
        }

        public void NewDelim(char[] newDel)
        {
            string temp = newDel.ToString(); 
            //temp=newDel.ToString();            
            if (temp.Length == 0)     
                {
                    this.StrDelimiter = " ";
                }
            else
                {
                 this.StrDelimiter = temp;
                }
            this.Tokenize();
                
        }
        public int CountTokens()
        {
            return this.tokens.Count;
        }

        public bool HasMoreTokens()
        {
            return (this.CurrIndex <= (this.tokens.Count - 1)) ;
        }
        public string NextToken()
        {
            string RetString="";
            
            if (this.CurrIndex <= (this.tokens.Count - 1)) 
            {
                RetString = Convert.ToString(this.tokens[this.CurrIndex]);
                this.CurrIndex = this.CurrIndex+1;
                return RetString;
                
            }
            else
            {
                return null;
            }
        }
    }
}




