using System;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Collections;
using System.IO;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.Domain.CriteriaLib.Helper
{
    [XmlRootAttribute("rootSv", IsNullable = false)]
    public class SvRoot
    {
        [XmlElement(ElementName = "SV")]
        public Sv sv;
        public SvRoot()
        {
            sv = new Sv();
        }
    }
}

        //public void GetSv(int svMode)
        //{
        //    Sv tmpSv = new Sv();
        //    if (this.sv.id > 0)
        //    {
        //        Connector connector = new Connector();
        //        connector.Mode = StringConst.svMode;
        //        connector.AddParam("opr", "getxmlsv");
        //        connector.AddParam("mode", svMode.ToString());
        //        connector.AddParam("svid", this.sv.id.ToString());
        //        connector.Execute();

        //        MemoryStream tmpStream = new MemoryStream();
        //        //System.Windows.Forms.MessageBox.Show(connector.GetRawString());
        //        if (connector.GetRawString().StartsWith("####"))
        //        {
        //            this.sv = null;
        //            return;
        //        }
        //        byte[] data = Encoding.GetEncoding("windows-1251").GetBytes(connector.GetRawString());
        //        tmpStream.Write(data, 0, data.Length);
        //        tmpStream.Position = 0;
        //        StreamReader tmpStreamReader = new StreamReader(tmpStream, Encoding.GetEncoding("windows-1251"));

        //        {
        //            XmlSerializer tmpSerializer = new XmlSerializer(typeof(SvRoot));//PenaltyList));
        //            this.sv = ((SvRoot)tmpSerializer.Deserialize(tmpStreamReader)).sv;

        //        }
        //        tmpStream.Close();
        //        tmpStreamReader.Close();
        //    }
        //    else
        //    if ((this.sv.dbId > 0) & (this.sv.number != String.Empty))
        //    {
        //        Connector connector = new Connector();
        //        connector.Mode = StringConst.svMode;
        //        connector.AddParam("opr", "getxmlsv");
        //        connector.AddParam("mode", svMode.ToString());
        //        connector.AddParam("svnmb", this.sv.number);
        //        connector.AddParam("dbid", this.sv.dbId.ToString());
        //        connector.Execute();

        //        MemoryStream tmpStream = new MemoryStream();
        //        //System.Windows.Forms.MessageBox.Show(connector.GetRawString());
        //        if (connector.GetRawString().StartsWith("####"))
        //        {
        //            this.sv = null;
        //            return;
        //        }
        //        byte[] data = Encoding.GetEncoding("windows-1251").GetBytes(connector.GetRawString());
        //        tmpStream.Write(data, 0, data.Length);
        //        tmpStream.Position = 0;
        //        StreamReader tmpStreamReader = new StreamReader(tmpStream, Encoding.GetEncoding("windows-1251"));

        //        {
        //            XmlSerializer tmpSerializer = new XmlSerializer(typeof(SvRoot));//PenaltyList));
        //            this.sv = ((SvRoot)tmpSerializer.Deserialize(tmpStreamReader)).sv;

        //        }
        //        tmpStream.Close();
        //        tmpStreamReader.Close();


        //    }
        //    else
        //    {
        //        this.sv=null;
        //        return;
        //    }
        //}


