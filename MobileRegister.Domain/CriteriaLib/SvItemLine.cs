﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace MobileRegister.Domain.CriteriaLib
{
    public class SvItemLine
    {
        public SvItem item;
        public int qty;
        public Double sum
        {          
            get { return ((Double) qty * this.item.Price[0].value); }
        }
    }
}
