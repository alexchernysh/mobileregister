﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Collections;
using System.IO;



namespace MobileRegister.Domain.CriteriaLib
{

    public class Sv : Criteria
    {
        [XmlAttribute]
        public String count;

        [XmlAttribute]
        public String sets;


        [XmlAttribute]
        public String number;


        [XmlAttribute]
        public Int32 dbId;

        [XmlAttribute]
        public String type;


        [XmlAttribute]
        public String svType;

        [XmlAttribute]
        public String createDate;

        [XmlAttribute]
        public String cshDate;

        [XmlAttribute]
        public String recDate;

        [XmlElement(ElementName = "PARTNER")]
        public List<Partner> partner;

        [XmlElement(ElementName = "FLIGHT")]
        public Flight flight;

        [XmlElement(ElementName = "ITEM")]
        public List<SvItem> Items;

        [XmlElement(ElementName = "CURRENCY")]
        public List<Currency_> CurrencyLst;

        public Sv()
        {
            id = 0;
            number = String.Empty;

        }
        public override String ToString()
        {
            return this.number;
        }

    }

}


