﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace MobileRegister.Domain.CriteriaLib
{
    public class Receipt :Criteria
    {
        [XmlAttribute]
        public string timestamp;

        [XmlElement(ElementName = "ITEMLINES")]
        public List<SvItemLine> ItemLines;

        //[XmlElement(ElementName = "PAYMENTS")]
        //public List<Payment> Payments;


        public override string ToString()
        {
            return id.ToString();
        }


    }
}
