﻿using System;
using System.Xml;
using System.Xml.Serialization;


namespace MobileRegister.Domain.CriteriaLib
{
    public class SvItemPrice
    {
        [XmlAttribute]
        public String name;
        [XmlAttribute]
        public Double value;
        [XmlAttribute]
        public String currName;
        [XmlAttribute]
        public int currId;

        
    }
}
