﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace MobileRegister.Domain.CriteriaLib
{
 
    public class Barcode:Criteria 

    {       [XmlAttribute]
            public string number;  

            public override string ToString()
            {
                return number.ToString();
            }

        }
}

