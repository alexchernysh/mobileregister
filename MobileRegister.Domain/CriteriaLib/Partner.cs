﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Collections;


namespace MobileRegister.Domain.CriteriaLib
{

    public class Partner : Criteria
    {
        [XmlAttribute(AttributeName = "partId")]
        public int PartId;

        [XmlAttribute(AttributeName = "partPref")]
        public string partPref;

        [XmlAttribute(AttributeName = "partNmb")]
        public int PartNmb;

        [XmlAttribute(AttributeName = "rank")]
        public int rank;

        [XmlAttribute(AttributeName = "companyId")]
        public int companyId;

        [XmlAttribute(AttributeName = "fName")]
        public string fName;

        [XmlAttribute(AttributeName = "sName")]
        public string sName;

        [XmlAttribute(AttributeName = "lName")]
        public string lName;

        public override string ToString()
        {
            throw new NotImplementedException();
        }
    }
}



   
