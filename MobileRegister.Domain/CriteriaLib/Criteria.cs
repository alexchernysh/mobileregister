﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

namespace MobileRegister.Domain.CriteriaLib
{
    public abstract class Criteria
    {
        private int fId;

        [XmlAttribute(AttributeName = "id")]
        
        public int id
        {
            get
            { return  fId;}            
            set
            { fId=value; }
            
        }
        public abstract override string ToString();
    }
}


