﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace MobileRegister.Domain.CriteriaLib
{

    public class Destination : Criteria
    {

        public Destination()
        {
            id = 0;
        }

        [XmlAttribute(AttributeName = "shortName")]
        public String shortName;

        [XmlAttribute(AttributeName = "fullName")]
        public String fullName;

        [XmlAttribute(AttributeName = "isPrinted")]
        public Boolean isPrinted;

        public override String ToString()
        {
            return this.shortName;
        }


    }
}
