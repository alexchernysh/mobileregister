﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace MobileRegister.Domain.CriteriaLib
{
    public class SvItem:Criteria 
    {

            [XmlAttribute]
            public int itemId;
            [XmlAttribute]
            public String number;
            [XmlAttribute]
            public String desc;

            [XmlAttribute]
            public String pickQty;
            [XmlAttribute]
            public String soldQty;

            [XmlAttribute]
            public String shelfNmb;

            [XmlAttribute]
            public String depName;

            [XmlAttribute]
            public int depId;

            [XmlElement(ElementName ="PRICE")]
            public List<SvItemPrice> Price;
            //<BARCODE id="167" number="80432402726" type="dutyfree" desc="empty" isBlocked="false" itemId="167" />

            [XmlElement(ElementName = "BARCODE")]
            public List<Barcode> Barcodes;

            
            public override string ToString()
            {
                return number;
            }

       
    }
}
