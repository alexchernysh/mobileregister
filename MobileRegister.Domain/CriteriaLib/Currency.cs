﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Collections;

namespace MobileRegister.Domain.CriteriaLib
{

    public class Currency_:Criteria
    {

        [XmlAttribute(AttributeName= "name")]
        public string name;
        [XmlAttribute]
        public Boolean isBase;
        [XmlAttribute]
        public String type;
        [XmlAttribute (AttributeName = "desc")]
        public String _desc;
        [XmlAttribute]
        public Double rate;

        public override string ToString()
        {
            return this._desc;
        }
    }
}

