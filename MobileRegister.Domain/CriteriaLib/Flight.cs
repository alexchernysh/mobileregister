﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace MobileRegister.Domain.CriteriaLib
{

    public class Flight : Criteria
    {

        [XmlAttribute(AttributeName = "desc")]
        public String _desc;

        [XmlAttribute(AttributeName = "number")]
        public String number;

        [XmlAttribute(AttributeName = "date")]
        public String _date;

        [XmlAttribute(AttributeName = "departTime")]
        public String departTime;

        [XmlElement(ElementName = "DESTINATION")]
        public Destination destination;

        [XmlElement(ElementName = "COMPANY")]
        public Company company;

        public Flight()
        {

        }

        public override String ToString()
        {
            return this.number;
        }


    }
}
