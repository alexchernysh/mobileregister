﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace MobileRegister.Domain.CriteriaLib
{
    public class Payment
    {
            /// <summary>
            /// сумма в базовой валюте
            /// </summary>
            private Double pValue;
            private Currency_ pCur;

            public Double valueBase
            {
                get { return pValue; }
                set { pValue = value; }
            }
            public Double valueLocal
            {
                get { return pValue / pCur.rate; }
                set { pValue = value * pCur.rate; }
            }

            public Currency_ currency
            {
                get  { return pCur; }
            }

            public Payment(Double vl, Currency_ cur)
            {
                pCur = cur;
                pValue = vl * pCur.rate;
            }

    }
}
