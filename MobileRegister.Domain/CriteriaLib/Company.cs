﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace MobileRegister.Domain.CriteriaLib
{

    public class Company : Criteria
    {

        [XmlAttribute(AttributeName = "name")]
        public String name;

        [XmlAttribute(AttributeName = "descE")]
        public String descE;

        [XmlAttribute(AttributeName = "descR")]
        public String descR;

        [XmlAttribute(AttributeName = "crewPrefix")]
        public string crewPrefix;

        public override String ToString()
        {
            return this.descR;
        }


    }
}
