﻿using System;
using System.Linq;
using System.Collections.Generic;
//using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Text;

namespace MobileRegister.Utility
{
    public class PlatformDetector
    {
        
        [DllImport("Coredll.dll", EntryPoint = "SystemParametersInfoW", CharSet = CharSet.Unicode)]
        static extern int SystemParametersInfo4Strings(uint uiAction, uint uiParam, StringBuilder pvParam, uint fWinIni);

        private enum SystemParametersInfoActions : uint
        {
            SPI_GETPLATFORMTYPE = 257, 
            SPI_GETOEMINFO = 258,
        }

        public static string GetOemInfo()
        {
            StringBuilder oemInfo = new StringBuilder(50);
            if (SystemParametersInfo4Strings((uint)SystemParametersInfoActions.SPI_GETOEMINFO,
                (uint)oemInfo.Capacity, oemInfo, 0) == 0)
                throw new Exception("Ошибка получения имени устройства");
            return oemInfo.ToString();
        }
    }

    public class EmulatorDetector
    {
        public const string MicrosoftEmulatorOemValue = "Microsoft DeviceEmulator";
        public static bool IsEmulator()
        {
            return PlatformDetector.GetOemInfo() == MicrosoftEmulatorOemValue;
        }
    }
}
