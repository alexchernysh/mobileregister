﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;


namespace MobileRegister.Utility
{
    internal class DBCreateController
    {

        #region sql create tables commands

        private static string sql_create_tbl_partner = @"CREATE TABLE 'partner'(                                
                                            'part_id' INTEGER,
                                            'crew_nmb' INTEGER,
                                            'name' TEXT,
                                            'is_admin' INTEGER)";

        private static string sql_create_tbl_item = @"CREATE TABLE 'item'(
                                            'itemid' INTEGER PRIMARY KEY,
                                            'plu' TEXT,
                                            'desc_r' TEXT,
                                            'desc_e' TEXT,
                                            'price' NUMERIC,
                                            'picked_qty' INTEGER,
                                            'sold_qty' INTEGER,
                                            'depid' INTEGER,
                                            'depname' TEXT,
                                            'order_id' INTEGER)";

        private static string sql_create_tbl_barcode = @"CREATE TABLE 'barcode'(                                
                                            'code' TEXT,                                
                                            'itemid' INTEGER)";

        private static string sql_create_tbl_rcptstate = @"CREATE TABLE 'rcptstate'(                                
                                            'state_id' INTEGER,
                                            'name' TEXT, 
                                            PRIMARY KEY ('state_id'))";

        //private static string sql_init_tbl_rcptstate = @"INSERT INTO TABLE rcptstate ('state_id','name') values (" +
        //                                CommonConst.rcpt_new_state.ToString() + ",'new'),(" +
        //                                CommonConst.rcpt_closed_state.ToString() + ",'closed'),(" +
        //                                CommonConst.rcpt_cancelled_state.ToString() + ",'cancelled')";

        private static string sql_create_tbl_receipt = @"CREATE TABLE 'receipt'(                                
                                            'rcpt_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                            'state_id' INTEGER, 
                                            'datestamp' TEXT)";

        private static string sql_create_tbl_currency = @"CREATE TABLE 'currency'(                                
                                            'curr_id' INTEGER, 
                                            'curr_name_lng' TEXT,
                                            'curr_name_shrt' TEXT,
                                            'is_base' INTEGER, 
                                            'rate' NUMERIC,
                                            PRIMARY KEY ('curr_id'))";


        private static string sql_create_tbl_payment = @"CREATE TABLE 'payment'(                                
                                            'payment_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                            'rcpt_id' INTEGER,
                                            'valuebase' NUMERIC,
                                            'valuelocal' NUMERIC,
                                            'curr_id' INTEGER)";


        private static string sql_create_tbl_rcptgood = @"CREATE TABLE 'rcptgood'(                                
                                            'good_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                                            'rcpt_id' INTEGER,
                                            'item_id' INTEGER,
                                            'qty' INTEGER,
                                            'discount' INTEGER)";


        private static string sql_create_idx_barcode = @"CREATE INDEX itemid_idx ON barcode(itemid)";

        private static string sql_create_idx_item_plu = @"CREATE INDEX item_plu_idx ON item(plu)";

        private static string sql_create_idx_good_rcpt = @"CREATE INDEX item_good_rcpt_idx ON rcptgood(rcpt_id)";

        private static string sql_create_idx_payment_rcpt = @"CREATE INDEX payment_rcpt_idx ON payment(rcpt_id)";

        #endregion

        public static void CreateDbStructure(DbConnection con, DbCommand command)
        {

            command.CommandText = sql_create_tbl_item;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_barcode;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_idx_barcode;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_currency;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_idx_item_plu;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_rcptgood;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_rcptstate;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_receipt;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_payment;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_idx_payment_rcpt;
            command.ExecuteNonQuery();

            command.CommandText = sql_create_idx_good_rcpt;
            command.ExecuteNonQuery();

            //command.CommandText = sql_init_tbl_rcptstate;
            //command.ExecuteNonQuery();

            command.CommandText = sql_create_tbl_partner;
            command.ExecuteNonQuery();
        }



    }
}
