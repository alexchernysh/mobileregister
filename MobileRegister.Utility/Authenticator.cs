﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.Domain.ConnectorLib;

namespace MobileRegister.Utility
{
    public class Authenticator :Connector
    {

        public bool IsLoginValid(out string errorMessage, string uName, string uPass )
        {
            URL = "http://192.168.2.210:34389/sky/servlet/SkyshopSystem";
            Mode = "login";
            AddParam("usrname", uName);
            AddParam("usrpwd", uPass);
            this.ExecuteAndParse();
            errorMessage = GetFieldAsString(0);
            if (!errorMessage.StartsWith("####"))
            {
                URL = errorMessage + ":" +
                GetFieldAsString(1) + ":" +
                GetFieldAsString(2);
                return true;
            }
            else return false;
            
        }
    }
}
