﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
//using System.Threading;
using MobileRegister.Domain.ConnectorLib;
using MobileRegister.Domain.CriteriaLib;
using MobileRegister.Domain.CriteriaLib.Helper;



namespace MobileRegister.Utility
{
    public class WayBillLoader
    {
        //private ThreadStart starter=new ThreadStart(MyFunction);
        private StreamReader reader;
        private XmlSerializer serializer;
        //private Sv sv;

        public WayBillLoader()
        {
        }



        public Sv Load(int number)//network load 
        {
            //WLanSwitch ws = new WLanSwitch();
            //ws.PowerOnWlan();
            Sv result = new Sv();
            string errorMessage = string.Empty;
            Authenticator auth = new Authenticator();
            if (auth.IsLoginValid(out errorMessage, "1", "1"))
            {                
                Connector connector = new Connector();
                connector.Mode = "sv";
                connector.AddParam("opr", "getxmlsv");
                connector.AddParam("mode", "5");
                connector.AddParam("svnmb", number.ToString());
                connector.AddParam("dbid", "1");
                connector.Execute();
                MemoryStream stream = new MemoryStream();
                if (!connector.GetRawString().StartsWith("####"))
                {
                    byte[] data = Encoding.GetEncoding("windows-1251").GetBytes(connector.GetRawString());
                    stream.Write(data, 0, data.Length);
                    stream.Position = 0;
                    StreamReader streamReader = new StreamReader(stream, Encoding.GetEncoding("windows-1251"));
                    XmlSerializer serializer = new XmlSerializer(typeof(SvRoot));
                    result = ((SvRoot)serializer.Deserialize(streamReader)).sv;
                    stream.Close();
                    streamReader.Close();
                }               
            }
           // ws.PowerOffWlan();
            return result;
        }

        public Sv Load(string wbpath)
        {
            reader = new StreamReader(wbpath, Encoding.GetEncoding("windows-1251"));
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "rootSv";
            xRoot.IsNullable = false;
            serializer = new XmlSerializer(typeof(SvRoot));
            return ((SvRoot)serializer.Deserialize(reader)).sv;
        }

        public void PopulateDataBase()
        {

        }


    }
}