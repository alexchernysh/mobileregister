﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.Utility
{
    public class PaymentCalculator
    {
        /// <summary>
        /// totalBase - стоимость товаров в чеке в базовой валюте
        /// </summary>
        private Double totalBase;
        /// <summary>
        /// paymentStack - стек платежей по чеку
        /// </summary>
        private Stack<Payment> paymentStack;
        /// <summary>
        /// stackSum - сумма платежей по чеку в базовой валюте
        /// </summary>
        private Double stackSum;

        public PaymentCalculator(Double total)
        {
            totalBase = total;
            paymentStack = new Stack<Payment>();
            stackSum = 0.0;
        }

        public void PutPayment(Payment payment)
        {
            paymentStack.Push(payment);
            stackSum = stackSum + payment.valueBase;
        }

        public Double GetRest(Currency_ currency)
        {
            Double tmpResult=(totalBase - stackSum)*currency.rate;
            return tmpResult > 0.0 ? tmpResult : 0.0;
        }

        public Double GetChange(Currency_ currency)
        {
            Double tmpResult =  (stackSum-totalBase)*currency.rate;
            return tmpResult > 0.0 ? tmpResult : 0.0;
        }

        public void DeletePayment()
        {
            var delPayment=paymentStack.Pop();
            stackSum = stackSum - 
                delPayment.valueBase;
        }

        public void Clear()
        {
            paymentStack.Clear();
            stackSum = 0.0;
        }

        public Double GetPaidSum()
        {
            return stackSum;
        }
    }
}
