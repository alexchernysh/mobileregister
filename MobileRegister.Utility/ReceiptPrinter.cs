﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bluebird.Printer;
using System.Runtime.InteropServices;

namespace MobileRegister.Utility
{
    public class ReceiptPrinter
    {
        List<string> receipt = new List<string>(); 
        Printer BBPrint = new Printer();
        const string empty5Line = "\n\n\n\n\n";

        public ReceiptPrinter()
        {
            
        }

        public void Open()
        {
            BBPrint.Open(0);


        }

        private void Print(string Txt)
        {
            BBPrint.PrintText(Txt, Printer.OPTION_DEFAULT);
           
        }

        private void PrintLine(string Txt)
        {
            this.Print(Txt + Environment.NewLine);
        }

        public void ClosePrinter()
        {
            BBPrint.Close();
        }
        /// <summary>
        /// Печатает содержимое буфера receipt на встроенный принтер PIDION
        /// </summary>
        public void PrintReceipt()
        {
            try
            {
                BBPrint.Open(0);
                BBPrint.RegisterFontType( Printer.FONTTYPE_40);
                BBPrint.Init();
                if ((receipt != null) && (receipt.Count > 0))
                {
                    foreach (string str in receipt)
                    {
                        PrintLine(str);
                    }
                    
                    PaperAdvance();
                    uint ret;
                    while ((ret = BBPrint.WaitUntilPrintEnd()) == Printer.ERROR_OVERHEAT)  // check printer state.
                    {
                        Thread.Sleep(100); // If printer is overheating, not working for 1 second.
                    }
                }
                BBPrint.Close();
            }
            catch
            {
                //какая-то обработка.......
            }               
        }

        public void AddString(string receiptLine)
        {
            receipt.Add(receiptLine);
        }

        private void PaperAdvance()
        {
            Print(empty5Line);
        }
    }
}
