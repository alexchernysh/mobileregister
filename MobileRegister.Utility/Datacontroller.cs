﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;
using System.IO;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.Utility
{
    public class DataController
    {
        //public Currency_ baseCurrency { get { return GetBaseCurrency(); } }
        //private SQLiteConnection con;
        //private string datafile;
        //private SQLiteCommand command;
        //private  string path;

        /// <summary>
        /// Отрефакторенные описания
        /// </summary>
        private static string DBPath = ApplicationSettings.CacheDir + "\\data.db";


        private static void DeleteDB()
        {
            if (File.Exists(DBPath))
            {
                File.Delete(DBPath);
            }
        }

        

//delete from your_table;    
//delete from sqlite_sequence where name='your_table';


        public static void MoveDB(string newPath)
        {
            if (File.Exists(DBPath))
            {
                File.Move(DBPath, newPath + "\\data.db");
                DBPath = newPath + "\\data.db";
            }
        }


        public static SQLiteConnection GetConnection()
        {
            if (!File.Exists(DBPath))
            {
                CreateDB();
            }
            return new SQLiteConnection("Data Source=" + DBPath);
        }

        public static void CreateDB()
        {
            if (!Directory.Exists(Path.GetDirectoryName(DBPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(DBPath));
            }

            if (!File.Exists(DBPath))
            {
                SQLiteConnection.CreateFile(DBPath);
            }
            try
            {
                using (SQLiteConnection conn = GetConnection())
                {
                    using (var comm = new SQLiteCommand(conn))
                    {
                        conn.Open();
                        SQLiteTransaction t = conn.BeginTransaction();

                        //comm.CommandText = "PRAGMA auto_vacuum=1;";
                        //comm.ExecuteNonQuery();

                        //comm.CommandText = "PRAGMA locking_mode=EXCLUSIVE; ";
                        //comm.ExecuteNonQuery();

                        comm.CommandText = "PRAGMA journal_mode=OFF; ";
                        comm.ExecuteNonQuery();

                        DBCreateController.CreateDbStructure(conn, comm);

                        t.Commit();
                        conn.Close();
                    }
                }
            }
            catch (Exception)
            {
            }
        
        }


        /// <summary>
        /// Отрефакторенные описания закончились
        /// </summary>



        public static int CurrentReceiptNumber
        {
            get { return CurrentReceiptID(); }
        }

        //public DataController()
        //{
           
        //    //path = System.IO.Path.
        //    //    GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        //    //datafile = Path.Combine(path, "data.db");

        //   // baseCurrency = GetBaseCurrency();
        //} 


        #region private methods



        #region sql commands

        //private static string Clear

        private static string InsertReceiptCommand()
        {
            DateTime stamp = DateTime.Now;
            return String.Format(@"insert into receipt(state_id, datestamp) values({0},""{1}"");", CommonConst.rcpt_new_state, stamp);
        }

        private static string InsertRcptgoodCommand(string[] values)
        {
            return String.Format(@"insert into rcptgood(rcpt_id,item_id, qty,discount) values({0},{1},{2},{3});", values);
        }



        private static string InsertPaymentCommand(int rcpt_id, Double valuebase, Double valuelocal, int cur_id)
        {
            return String.Format(@"insert into payment(rcpt_id,valuebase, valuelocal,curr_id) values({0},{1},{2},{3});",
                rcpt_id.ToString(), valuebase.ToString("F2"), valuelocal.ToString("F2"), cur_id.ToString());
        }

        private static string UpdateRcptgoodCommand(string[] values)
        {
            return String.Format(@"update rcptgood set qty={2} where rcpt_id={0} and item_id={1};", values);
        }

        private static string CancelCurrentReceiptCommand()
        {
            return @"update receipt set state_id=" + CommonConst.rcpt_cancelled_state + " where rcpt_id=" + CurrentReceiptNumber;
        }

        private static string CloseCurrentReceiptCommand()
        {
            return @"update receipt set state_id=" + CommonConst.rcpt_closed_state + " where rcpt_id=" + CurrentReceiptNumber;
        }

        private static string UpdateItemQtyCommand(int qty, int itemid)
        {
            return String.Format( @"update item set sold_qty=sold_qty+{0} where itemid={1}",
                qty.ToString(),itemid.ToString());
        }

        private static string UpdatePaymentCommand(Double valueBase, Double valueLocal, int paymentId)
        {
            return String.Format
                (@"update payment set valuebase=valuebase+{0}, valuelocal=valuelocal+{1} where payment_id={2}", valueBase,valueLocal,paymentId);
        }
        
        private static string DeleteItemCommand(string code)
        {
            return String.Format(@"delete from rcptgood where rcpt_id={0} and item_id=(select itemid from item where plu=""{1}"")",
                CurrentReceiptNumber.ToString(), code);
        }

        #endregion


        private static  bool TableExists(String tableName, SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.CommandText = "SELECT COUNT(*) AS QtRecords FROM sqlite_master WHERE type = 'table' AND name = @name";
                cmd.Parameters.AddWithValue("@name", tableName);
                if (Convert.ToInt32(cmd.ExecuteScalar()) == 0)
                    return false;
                else
                    return true;
            }
        }

        private static int CurrentReceiptID()
        {
            int id;
            using (SQLiteConnection conn = GetConnection())
            {
                
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = "select MAX(rcpt_id) from receipt";
                    var obj = comm.ExecuteScalar();
                    if ((obj==null)||(obj==DBNull.Value))
                        id=1;
                    else 
                        id=(Convert.ToInt32( obj));
                }
                conn.Close();
            }
            return id;
        }

        private static int CurrentReceiptState()
        {
            int state_id;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = "select state_id from receipt where rcpt_id=@currcpt";
                    var dParm = new SQLiteParameter(DbType.Int32);
                    comm.Parameters.Add(new SQLiteParameter("@currcpt", CurrentReceiptID()));
                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                        state_id = CommonConst.rcpt_closed_state;
                    else
                        state_id = (Convert.ToInt32(obj));
                }
                conn.Close();
            }
            return state_id;
        }

        private static bool CreateReceipt()
        {
            bool result = false;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    //using (var transaction = conn.BeginTransaction())
                    //{
                        try
                        {
                            comm.CommandText = InsertReceiptCommand();
                            comm.ExecuteNonQuery();
                        //    AddItemToRcpt(itemid);
                            //transaction.Commit();
                            result = true;
                        }
                        catch (Exception ex)
                        {
                            //transaction.Rollback();
                            result = false;
                        }
                   // }
                }
                conn.Close();
            }
            return result;


        }


        /// <summary>
        /// Достаточно ли товара???
        /// </summary>
        /// <param name="itemid">id товара</param>
        /// <param name="qty">сколько хотим продать</param>
        /// <returns></returns>
        public static bool IsEnoughQty(string plu, int qty)
        {
            bool result=false;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = String.Format("SELECT picked_qty-sold_qty FROM item  WHERE plu ={0}", plu);
                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                        result = false;
                    else
                        result = (Convert.ToInt32(obj) -
                            ((CurrentReceiptState() != CommonConst.rcpt_new_state) ? 0 : GetItemQtyInCurrReceipt(plu))
                            >= qty);
                }
                conn.Close();
            }
            return result;
        }

        private static int FindPayment(Payment payment)
        {
            int id;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    //CREATE TABLE 'payment'(                                
                    //                        'payment_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                    //                        'rcpt_id' INTEGER,
                    //                        'valuebase' NUMERIC,
                    //                        'valuelocal' NUMERIC,
                    //                        'curr_id' INTEGER)
                    comm.CommandText = String.Format(
                        "SELECT payment_id FROM payment WHERE curr_id={0} AND rcpt_id={1}", payment.currency.id.ToString(),
                             CurrentReceiptID().ToString());
                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                        id = 0;
                    else id = (Convert.ToInt32(obj));
                }
                conn.Close();
            }
            return id;
        }
        
        /// <summary>
        /// Возвращает количество товара в текущем чеке по itemId
        /// </summary>
        /// <param name="itemId">itemid товара</param>
        /// <returns></returns>
        private static int GetItemQtyInCurrReceipt(int itemId)
        {
            int qty;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = String.Format(
                        "SELECT qty FROM rcptgood WHERE rcptgood.item_id={0} AND rcptgood.rcpt_id={1}", itemId.ToString(),
                             CurrentReceiptID().ToString());
                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                        qty = 0;
                    else qty = (Convert.ToInt32(obj));
                }
                conn.Close();
            }
            return qty;
        }

        
        /// <summary>
        ///  Возвращает количество товара в текущем чеке по коду Plu
        /// </summary>
        /// <param name="plu">код товара</param>
        /// <returns></returns>
        private static int GetItemQtyInCurrReceipt(string plu)
        {
            int qty;


            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();

                    comm.CommandText = String.Format(
                       "SELECT qty FROM rcptgood, item WHERE rcptgood.item_id=item.itemid and plu={0} and rcptgood.rcpt_id={1}",
                           plu, CurrentReceiptID().ToString());
                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                        qty = 0;
                    else qty = (Convert.ToInt32(obj));
                }
                conn.Close();
            }


            return qty;
        }

        /// <summary>
        /// Выполняет поиск товара по коду PLU или по штрихкоду
        /// </summary>
        /// <param name="code"></param>
        /// <returns>SvItem - найденный товар. Если товар не найден, то возвращает пустой SvItem c ItemID=0</returns>
        private static SvItem FindItem(string code)
        {
            SvItem tmpItem = new SvItem();
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();

                    if (code.Length > 6)
                    {
                        comm.CommandText = "select item.itemid,plu,desc_r,price from item, barcode where " +
                            "barcode.code=" + code + " and barcode.itemid=item.itemid";
                    }
                    else
                    {
                        comm.CommandText = "select itemid,plu,desc_r,price from item where plu=" + code;
                    }
                    using (SQLiteDataReader reader = comm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            //tmpReturn = new string[] { reader.GetInt32(0).ToString(), reader.GetString(1), reader.GetString(2), reader.GetDouble(3).ToString() };
                            while (reader.Read())
                            {
                                tmpItem.itemId = reader.GetInt32(0);
                                tmpItem.number = reader.GetString(1);
                                tmpItem.desc = reader.GetString(2);
                                tmpItem.Price = new List<SvItemPrice>();
                                tmpItem.Price.Add(new SvItemPrice() { value = reader.GetDouble(3) });
                            }
                        }
                    }

                }
                conn.Close();
            }

            return tmpItem;
        }  

        /// <summary>
        /// Добавляет товар в чек, если его там нет, или увеличивает количество на единицу, если такой товар уже есть в чеке
        /// </summary>
        /// <param name="itemId">int ItemID добавляемого товара</param>
        /// <param name="useTransaction">- удалено</param> 
        private static void AddItemToRcpt(int itemId)//, bool useTransaction
        {
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    int itemqty = GetItemQtyInCurrReceipt(itemId);
                    if (itemqty == 0)
                        comm.CommandText = 
                            InsertRcptgoodCommand(new string[] { CurrentReceiptID().ToString(), 
                                itemId.ToString(), "1", "0" });
                    else
                        comm.CommandText = 
                            UpdateRcptgoodCommand(new string[] { CurrentReceiptID().ToString(),
                         itemId.ToString(), (GetItemQtyInCurrReceipt(itemId) + 1).ToString() });
                    comm.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        #endregion

        #region public methods

        public static void ClearData()
        {
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    using (var transaction = conn.BeginTransaction())
                    {
                        comm.CommandText = @"delete from currency; delete from barcode; delete from item; delete from partner; delete from payment; delete from rcptgood; delete from receipt";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='currency'";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='barcode'";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='item'";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='partner'";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='payment'";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='rcptgood'";
                        comm.ExecuteNonQuery();

                        comm.CommandText = @"delete from sqlite_sequence where name='receipt'";
                        comm.ExecuteNonQuery();

                        transaction.Commit();
                    }
                }
                conn.Close();
            }
            
        }

        public static void InitialLoadData(Sv sv)
        {
            //DBInitController dbic = new DBInitController(sv, command, con);
            ClearData();
            DBInitController.LoadData(sv);
        }


        /// <summary>
        /// Возвращает текущий чек (для печати)
        /// </summary>
        /// <returns></returns>
        public static Receipt GetCurrentReceipt()
        {
            Receipt tmpReceipt;
            SvItemLine tmpItemLine;
            SvItem tmpItem;
            List<SvItemPrice> tmpPriceList;
            SvItemPrice tmpPrice;
            List<SvItemLine> tmpItemLineList = new List<SvItemLine>();
            //baseCurrency = GetBaseCurrency();
            try
            {

                int CurReceiptId = CurrentReceiptID();
                using (SQLiteConnection conn = GetConnection())
                {
                    using (var comm = new SQLiteCommand(conn))
                    {
                        conn.Open();

                        comm.CommandText = "select item.itemid, item.plu, item.desc_r, item.price, rcptgood.qty from item, rcptgood" +
                            " where item.itemid=rcptgood.item_id and  " +
                                "rcptgood.rcpt_id=" + CurReceiptId.ToString();


                        using (SQLiteDataReader reader = comm.ExecuteReader())
                        {
                            var baseCurrency = GetBaseCurrency();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    tmpPrice = new SvItemPrice() { currId = baseCurrency.id, name = baseCurrency.name, currName = baseCurrency._desc, value = reader.GetDouble(3) };
                                    tmpPriceList = new List<SvItemPrice>();
                                    tmpPriceList.Add(tmpPrice);

                                    tmpItem = new SvItem()
                                    {
                                        itemId = reader.GetInt32(0),
                                        number = reader.GetString(1),
                                        desc = reader.GetString(2),
                                        Price = tmpPriceList
                                    };
                                    tmpItemLine = new SvItemLine() { qty = reader.GetInt32(4), item = tmpItem };
                                    tmpItemLineList.Add(tmpItemLine);
                                }
                            }
                        }


                    }
                    conn.Close();
                }
                tmpReceipt = new Receipt() { id = CurReceiptId, timestamp = DateTime.Now.ToString(), ItemLines = tmpItemLineList };
            }
            catch (Exception ex)
            {                
                tmpReceipt = null;
            }
            return tmpReceipt;
        }

        public static void DeletePayments()
        {
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = "delete from payment where rcpt_id=" + CurrentReceiptID().ToString();
                    comm.ExecuteNonQuery();
                }
                conn.Close();
            }            
        }

        public static bool IsCurrentReceiptOpen()
        {
            return (CurrentReceiptState() == CommonConst.rcpt_new_state);
        }

        public static void DeleteItem(string code)
        {
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = DeleteItemCommand(code);
                    comm.ExecuteNonQuery();
                }
                conn.Close();
            } 
        }

        /// <summary>
        /// Если есть открытый чек, то добавляет товар к текущему чеку, 
        /// Если нет открытого, чека, то создает чек идобавляет к нему товар
        /// Возвращает экземпляр добавленного SvItem, если товар успешно добавлен,
        /// если товар не найден, то возвращает SvItem с itemId=0, 
        /// если нет необходимого количества, то возвращает SvItem с itemId=-1
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static SvItem AddItem(string code)
        {
            SvItem tmpItem = FindItem(code);

            if (tmpItem.itemId != 0)
            {
                if (IsEnoughQty(code, 1))
                {
                    if (CurrentReceiptState() == CommonConst.rcpt_new_state)
                    {
                        AddItemToRcpt(tmpItem.itemId);
                    }
                    else
                    {
                        if ( CreateReceipt() )
                        AddItemToRcpt(tmpItem.itemId);                       
                    }
                }
                else
                {
                    tmpItem.itemId = -1;
                    return tmpItem;
                }

            }
            return tmpItem;
        }

        /// <summary>
        /// Возвращает табельный номер ответственного за торговлю
        /// </summary>
        /// <returns>int - табельный номер, или -1, в случае ошибки</returns>
        public static int GetParnerNumber()
        {
            int nmb;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = "SELECT crew_nmb FROM partner";
                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                        nmb = -1;
                    else

                        nmb = (Convert.ToInt32(obj));
                }
                conn.Close();
            }
            return nmb;
        }

        /// <summary>
        /// Closes current receipt
        /// </summary>
        public static void CloseReceipt()
        {
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    
                    using (var transaction = conn.BeginTransaction())
                    {
                        try
                        {
                            // ---добавлен метод AddPayment!!!!----22.07.13
                            //comm.CommandText = InsertPaymentCommand(CurrentReceiptNumber, GetTotal(),
                            //    GetTotal(), GetBaseCurrency().id);
                            //comm.ExecuteNonQuery();
                            // ---добавлен метод AddPayment!!!!----22.07.13

                            //increase value of item.sold_qty field for all receipt items

                            //если заплачено больше чем надо, то добавить строку со сдачей (<0)

                            var change = GetTotal() - GetCurrentPaymentSum();
                            if (change != 0.0)
                            {
                                var payment = new Payment(change, GetBaseCurrency());
                                AddPayment(payment, comm);
                            }
                               

                            Receipt rcp = GetCurrentReceipt();
                            foreach (var il in rcp.ItemLines)
                            {
                                comm.CommandText = UpdateItemQtyCommand(il.qty, il.item.itemId);
                                comm.ExecuteNonQuery();
                            }

                            //close Receipt
                            comm.CommandText = CloseCurrentReceiptCommand();
                            comm.ExecuteNonQuery();

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                        }
                    }
                }
                conn.Close();
            }

        }


        public static void AddPayment(Payment payment)
        {
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open(); 
                    int PaymentId=FindPayment(payment);
                    if (PaymentId == 0)
                    {
                        comm.CommandText =
                            InsertPaymentCommand(CurrentReceiptNumber,
                                                payment.valueBase,
                                                payment.valueLocal,
                                                payment.currency.id);                  }
                    else
                    {
                        comm.CommandText =
                            UpdatePaymentCommand(payment.valueBase, payment.valueLocal, PaymentId);
                    }
                    comm.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        private static void AddPayment(Payment payment, SQLiteCommand comm)
        {
            comm.CommandText =
                        InsertPaymentCommand(CurrentReceiptNumber,
                                            payment.valueBase,
                                            payment.valueLocal,
                                            payment.currency.id);
            comm.ExecuteNonQuery();
        }

        /// <summary>
        /// Отмена текущего открытого чека (записывает в поле state_id таблицы receipt признак отмены)
        /// </summary>
        public static void CancelReceipt()
        {
            //1.Записать признак отмененного чека в текущем чеке в таблице Receipt
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = CancelCurrentReceiptCommand();
                    comm.ExecuteNonQuery();
                }
                conn.Close();
            }
        }



        /// <summary>
        /// Метод возвращает сумму стоимостей всех товаров в текущем чек
        /// </summary>
        /// <returns></returns>
        public static Double GetTotal()
        {
            Double total;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = "SELECT SUM(rcptgood.qty* item.price) FROM " +
                                          " rcptgood, item,receipt where item.itemid=rcptgood.item_id and rcptgood.rcpt_id=receipt.rcpt_id" +
                                          " and receipt.state_id=" + CommonConst.rcpt_new_state +
                                          " and rcptgood.rcpt_id=" + CurrentReceiptNumber;

                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                    {
                        total = 0.0;
                    }
                    else total = (Convert.ToDouble(obj));
                }
                conn.Close();
            }
            return total;
        }


        /// <summary>
        /// Метод возвращает сумму оплат в текущем чеке
        /// </summary>
        /// <returns></returns>
        private static Double GetCurrentPaymentSum()
        {
            Double total;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText =
                      "SELECT SUM(valuebase) FROM " +
                      " payment where rcpt_id=" + CurrentReceiptNumber;

                    var obj = comm.ExecuteScalar();
                    if ((obj == null) || (obj == DBNull.Value))
                    {
                        total = 0.0;
                    }
                    else total = (Convert.ToDouble(obj));
                }
                conn.Close();
            }
            return total;
        }

        public static Currency_ GetBaseCurrency()
        {
            Currency_ tmpCur = new Currency_();
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();

                    comm.CommandText = "select curr_id,curr_name_lng,curr_name_shrt from currency where is_base=1";

                    using (SQLiteDataReader reader = comm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                tmpCur.id = reader.GetInt32(0);
                                tmpCur._desc = reader.GetString(1);
                                tmpCur.name = reader.GetString(2);
                                tmpCur.isBase = true;
                                tmpCur.rate = 1;
                            }
                        }
                    }
                }
                conn.Close();
            }
             
            return tmpCur;
        }

        public static List<Currency_> GetCurrencyDict()
        {
            List<Currency_> tmpCurList = new List<Currency_>();

            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();

                    comm.CommandText = "select curr_id,curr_name_lng,curr_name_shrt,is_base,rate from currency order by is_base desc";

                    using (SQLiteDataReader reader = comm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Currency_ tmpCur = new Currency_();
                                tmpCur.id = reader.GetInt32(0);
                                tmpCur._desc = reader.GetString(1);
                                tmpCur.name = reader.GetString(2);
                                tmpCur.isBase = reader.GetBoolean(3);
                                tmpCur.rate = reader.GetDouble(4);
                                tmpCurList.Add(tmpCur);
                            }
                        }
                    }
                }
                conn.Close();
            }
            return tmpCurList;

        }

        private static Currency_ GetCurrencyByID(int ident)
        {
            Currency_ tmpCur=null;
            using (SQLiteConnection conn = GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    comm.CommandText = String.Format("select * from currency where curr_id={0}", ident.ToString());
                    using (SQLiteDataReader reader = comm.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                tmpCur = new Currency_();
                                tmpCur.id = reader.GetInt32(0);
                                tmpCur._desc = reader.GetString(1);
                                tmpCur.name = reader.GetString(2);
                                tmpCur.isBase = reader.GetBoolean(3);
                                tmpCur.rate = reader.GetDouble(4);
                            }
                        }

                    }
                }
                conn.Close();
            }
            return tmpCur;
        }
        #endregion

    }
}
