﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace MobileRegister.Utility
{
    class WLanSwitch
    {
        [DllImport("coredll.dll", SetLastError = true)]
        static extern int DevicePowerNotify(string name, DevicePowerState state, int flags);

        [DllImport("coredll.dll", SetLastError = true)]
        static extern int SetDevicePower(string pvDevice, int dwDeviceFlags, DevicePowerState DeviceState);

        [DllImport("coredll.dll", SetLastError = true)]
        static extern int GetDevicePower(string pvDevice, int dwDeviceFlags, ref DevicePowerState pDeviceState);

        private enum DevicePowerState : int
        {
            Unspecified = -1,
            D0 = 0, // Full On: full power, full functionality 
            D1, // Low Power On: fully functional at low power/performance 
            D2, // Standby: partially powered with automatic wake 
            D3, // Sleep: partially powered with device initiated wake 
            D4, // Off: unpowered 
        }

        private const int POWER_NAME = 0x00000001;
        string driver = "{98C5250D-C29A-4985-AE5F-AFE5367E5006}\\SWLD246C1";
    
        public void PowerOnWlan()
        {           
            SetDevicePower(driver, POWER_NAME,  DevicePowerState.D0);
        }
        public void PowerOffWlan()
        {
            SetDevicePower(driver, POWER_NAME, DevicePowerState.D4);
        }
            




    //private static string FindDriverKey() 
    //      { 
    //         string ret = string.Empty; 

    //         //#define PMCLASS_NDIS_MINIPORT      TEXT("{98C5250D-C29A-4985-AE5F-AFE5367E5006}") 
    //         //(From "c:\Program Files (x86)\Windows Mobile 6 SDK\PocketPC\Include\Armv4i\pm.h") 
    //         string WiFiDriverClass= "{98C5250D-C29A-4985-AE5F-AFE5367E5006}"; 

    //         foreach (string tmp in Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Control\\Power\\State", false).GetValueNames()) 
    //         { 
    //           if (tmp.Contains(WiFiDriverClass)) 
    //           { 
    //             ret = tmp; 
    //             break; 
    //           } 
    //         } 

    //         return ret; 
    //      }
    
    
    }
}
