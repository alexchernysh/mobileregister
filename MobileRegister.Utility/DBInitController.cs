﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Data.SQLite;
using MobileRegister.Domain.CriteriaLib;

namespace MobileRegister.Utility
{
    internal class DBInitController
    {
        //private Sv sv;
        //private DbCommand command;
        //private DbConnection con;

        //public DBInitController( Sv sv_, DbCommand cmd, DbConnection cnct )
        //{
        //    sv = sv_;
        //    command = cmd;
        //    con = cnct;
        //}

        #region insert commands
        private static string InsertPartnerCommand(string[] values)
        {
            return String.Format(@"insert into partner(part_id,crew_nmb, name,is_admin) values({0},{1},""{2}"",{3});", values);
        }
        private static string InsertCurrencyCommand(string[] values)
        {
            return
                String.Format(@"insert into currency(curr_id, curr_name_lng, curr_name_shrt, is_base, rate) values({0},""{1}"",""{2}"",{3},{4});", values);

        }

        private static string InsertItemCommand(string[] values)
        {
            return
                String.Format(@"insert into item(itemid, plu, desc_r, desc_e, price ,picked_qty,
                sold_qty, depid, depname, order_id) values({0},{1},""{2}"",""{3}"",{4},{5},{6},{7},""{8}"",{9});", values);
        }

        private static string InsertBarcodeCommand(string[] values)
        {
            return
                String.Format(@"insert into barcode(code, itemid) values(""{0}"",{1});", values);
        }
        #endregion



        public static void LoadData(Sv sv)
        {

            using (SQLiteConnection conn = DataController.GetConnection())
            {
                using (var comm = new SQLiteCommand(conn))
                {
                    conn.Open();
                    using (var transaction = conn.BeginTransaction())
                    {
                        try
                        {
                            #region items & barcodes loading
                            if (sv.Items == null || sv.Items.Count == 0)
                            {
                                transaction.Rollback();
                                throw new Exception("Нет товаров в ТТН");
                            }
                            //
                            foreach (SvItem tmpItem in sv.Items)
                            {
                                comm.CommandText = InsertItemCommand(new string[]{
                                    tmpItem.itemId.ToString(), tmpItem.number, tmpItem.desc, 
                                    tmpItem.desc, tmpItem.Price[0].value.ToString(),
                                    tmpItem.pickQty, tmpItem.soldQty, tmpItem.depId.ToString(), 
                                    tmpItem.depName,"0"});
                                comm.ExecuteNonQuery();
                                if (sv.Items == null || sv.Items.Count == 0)
                                {
                                    throw new Exception("Нет товаров в ТТН");
                                }
                                foreach (Barcode tmpBc in tmpItem.Barcodes)
                                {
                                    comm.CommandText = InsertBarcodeCommand(new string[] { tmpBc.number, tmpItem.itemId.ToString() });
                                    comm.ExecuteNonQuery();
                                }
                            }
                            #endregion
                            #region currency rates loading
                            if (sv.CurrencyLst == null || sv.CurrencyLst.Count == 0)
                            {
                                throw new Exception("Нет курса валют");
                            }

                            foreach (Currency_ tmpCur in sv.CurrencyLst)
                            {
                                comm.CommandText = InsertCurrencyCommand(new string[] { tmpCur.id.ToString(), tmpCur._desc, tmpCur.name, Convert.ToInt16(tmpCur.isBase).ToString(), tmpCur.rate.ToString() });
                                comm.ExecuteNonQuery();
                            }
                            #endregion
                            #region partner loading
                            const string not_admin = "0";
                            if (sv.partner == null || sv.partner.Count != 1) throw new Exception("Ошибка загрузки бригады");

                            Partner p = sv.partner[0];
                            string name = p.lName + " " + p.fName.Substring(0, 1) + ". " + p.fName.Substring(0, 1) + ". ";
                            comm.CommandText = InsertPartnerCommand(new string[] { p.PartId.ToString(), p.PartNmb.ToString(), name, not_admin });
                            comm.ExecuteNonQuery();
                            #endregion
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                        }
                    }
                }
                conn.Close();
            }
            
        }

    }
}
