﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Reflection;

namespace MobileRegister.Utility
{
    public static class ApplicationSettings
    {
        [System.Runtime.InteropServices.DllImport("coredll.dll")]
        private static extern int SHCreateShortcut(StringBuilder szShortcut, StringBuilder szTarget);


        #region Fields
        public static string AppPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);


        #endregion Fields

        #region Constructors 

     static ApplicationSettings()
     {
         LoadSettings();
            //LoadColors();
     }

       



    #endregion Constructors 
        #region Properties 


         public static bool RunOnStartUp
         {
             get
             {
                 return System.IO.File.Exists(@"\Windows\StartUp\MobileRegister.lnk");
             }
             set
             {
                 if (value)
                 {
                     StringBuilder shortcut = new StringBuilder(@"\Windows\StartUp\MobileRegister.lnk");
                     StringBuilder target;
                     //if (ApplicationSettings.AppPath.IndexOf(' ') > 0)
                     //{
                     //    target = new StringBuilder("\"" + ApplicationSettings.AppPath + "\\MobileRegister.exe" + "\"" + " /BackGround");
                     // }
                     // else
                     // {
                         target = new StringBuilder(ApplicationSettings.AppPath + "\\MobileRegister.exe");
                     // }

                     SHCreateShortcut(shortcut, target);
                 }
                 else
                 {
                     if (RunOnStartUp)
                     {
                         System.IO.File.Delete(@"\Windows\StartUp\MobileRegister.lnk");
                     }
                 }
             }
         }

         private static string _CacheDir;
         public static string CacheDir
         {
             get
             {
                 //временная подстановка до полного включения в работу Appsettings
                 if (string.IsNullOrEmpty(_CacheDir))
                     _CacheDir=AppPath;
                 return _CacheDir;
             }
             set
             {
                 if (string.IsNullOrEmpty(value))
                 {
                     value = AppPath;
                 }
                 if (!string.IsNullOrEmpty(_CacheDir) && value != _CacheDir)
                 {
                     //LocalStorage.DataBaseUtility.MoveDB(value);
                 }
                 _CacheDir = value;
             }
         }


        #endregion
        #region Methods
         public static void LoadSettings()
         {
             //ConfigurationSettings.LoadConfig();
             //IFormatProvider format = new System.Globalization.CultureInfo(1033);
             try
             {
                 if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["CacheDir"]))
                 {
                     CacheDir = ConfigurationSettings.AppSettings["CacheDir"];
                 }
                 else
                 {
                     CacheDir = AppPath;
                 }

             }
             catch (Exception ex)
             {
                 System.Diagnostics.Debug.WriteLine(ex.Message);
             }

         }

        #endregion Methods
    }
}

