﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace MobileRegister.Utility
{
    public class WinMenuSwitch
    {
        private const int SW_HIDE = 0x00;
        private const int SW_SHOWNORMAL = 0x01;
        private const int SW_SHOWMINIMIZED = 0x02;
        private const int SW_SHOWMAXIMIZED = 0x03;
        private const int SW_SHOWNOACTIVATE = 0x04;
        private const int SW_SHOW = 0x05;
        private const int SW_MINIMIZE = 0x06;
        private const int SW_SHOWMINNOACTIVATE = 0x07;
        private const int SW_SHOWNA = 0x08;
        private const int SW_RESTORE = 0x09;
        private const int GW_HWNDNEXT = 0x02;
        private const int GW_HWNDPREV = 0x03;


        [DllImport("coredll.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("coredll.dll", CharSet = CharSet.Auto)]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        [DllImport("coredll.dll", CharSet = CharSet.Auto)]
        private static extern bool EnableWindow(IntPtr hwnd, bool enabled);



        public static void ShowTopStatusbar(bool show)
        {
            IntPtr h;
            try
            {
                h = FindWindow("HHTaskBar", "");
                if (show)
                {
                    ShowWindow(h, SW_SHOWNORMAL);
                    EnableWindow(h, true);
                }
                else
                {
                    ShowWindow(h, SW_HIDE);
                    EnableWindow(h, false);
                }
            }
            finally
            {
                h = IntPtr.Zero;
            }
        }
    }
}

